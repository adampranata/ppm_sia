<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8" />
  <base href="<?php echo base_url(); ?>">
  <!-- Set the viewport width to device width for mobile -->
  <meta name="viewport" content="width=device-width" />

  <title><?php echo $title; ?> - PPM </title>
  <link rel="stylesheet" href="css/foundation.min.css" >
  <link rel="stylesheet" href="css/foundation.css" >
  <link rel="stylesheet" href="css/normalize.css" >
  <link rel="shortcut icon" href="img/favicon.ico"/>
  <script src="js/jquery.js"></script>

 
  


</head>
<body class="form">  
  <nav class="top-bar">
    <ul class="title-area">
      <!-- Title Area -->
      <li class="name">
        <h1>
          <a href="#" data-reveal-id="leaveModal">
            DATABASE - PPM YOGYAKARTA
          </a>
        </h1>
      </li>
    </ul>
  </nav>

        <div class="row">
            <div class="container">
              <div class="large-8 columns">
                <?php echo $content; ?> 
              </div>
            </div>
        <div class="large-4 columns">
               <?php echo $this->load->view('terms'); ?>
        </div>
      </div>
    <?php echo $this->load->view('footer'); ?>
    


<div id="leaveModal" class="reveal-modal">
  <h3>Peringatan!</h3>
  <h4>Anda Yakin Ingin Meninggalkan Form Registrasi Anda ?</h4>
  <a href="#" class="button radius centre">Ya!</a>
  <a class="close-reveal-modal">&#215;</a>

    <script src="js/foundation/foundation.js"></script>
  
  <script src="js/foundation/foundation.dropdown.js"></script>
  
  <script src="js/foundation/foundation.placeholder.js"></script>
  
  <script src="js/foundation/foundation.forms.js"></script>
  
  <script src="js/foundation/foundation.alerts.js"></script>
  
  <script src="js/foundation/foundation.magellan.js"></script>
  
  <script src="js/foundation/foundation.reveal.js"></script>
  
  <script src="js/foundation/foundation.tooltips.js"></script>
  
  <script src="js/foundation/foundation.clearing.js"></script>
  
  <script src="js/foundation/foundation.cookie.js"></script>
  
  <script src="js/foundation/foundation.joyride.js"></script>
  
  <!--<script src="js/foundation/foundation.orbit.js"></script>-->
  
  <script src="js/foundation/foundation.section.js"></script>
  
  <script src="js/foundation/foundation.topbar.js"></script>

  

<!--<script src="js/foundation/foundation.orbit.js"></script>-->
<script src="js/foundation/foundation.alerts.js"></script>
<script src="js/foundation/foundation.js"></script>
  
  <!-- Included JS Files (Compressed) -->
  
  <script src="js/vendor/jquery.js"></script>
  <script src="js/foundation.min.js"></script>
  <script src="js/vendor/zepto.js"></script>
  
  <!-- Initialize JS Plugins -->
  <script src="javascripts/app.js"></script>

  <script>
    $(document).foundation();
  </script>

 <!-- <script type="text/javascript">
     $(window).load(function() {
         $('#slider').orbit();
     });
  </script>-->

  <script>
  document.write('<script src=/js/vendor/'
  +('__proto__' in {} ? 'zepto' : 'jquery')
  + '.js><\/script>');
  </script>
</body>
</html>