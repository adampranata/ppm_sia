<section>
      <p class="title" data-section-title><a href="#">Informasi Pendaftaran</a></p>
      <div class="content" data-section-content>
        <section class="row">
          <section class="small-6 columns">
            <div>
                    <table>
                        <thead>
                            <tr>
                                <th>ID_Pendaftar</th>
                                <th>Tahap 1</th>
                                <th>Tahap 2</th>
                                <th>Tahap 3</th>
                                <th>Tahap 4</th>
                            </tr>
                        </thead>
                        <tbody>q
                            <?php foreach($pendaftar => $list): ?>
                            <tr>
                                <td><?php echo $list('id_daftar'); ?></td>
                                <td><?php echo $list['tahap_1']; ?></td>
                                <td><?php echo $list['tahap_2']; ?></td>
                                <td><?php echo $list['tahap_3']; ?></td>
                                <td><?php echo $list['tahap_4']; ?></td>
                            </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
          </section>

          <section class="small-6 columns">
            <section class="panel">
              <h4 class="hide-for-small">Pesan Pendaftar<hr/></h4>
              
              <table>
                        <thead>
                            <tr>
                                <th>ID_Pendaftar</th>
                                <th>Pesan</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach($message as $key => $daftar): ?>
                            <tr>
                                <td><?php echo $daftar['id_daftar']; ?></td>
                                <td><?php echo $daftar['pesan']; ?></td>
                                <td><input type="button" name="" value=""></td>
                            </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
            </section>
 
            <section class="row">
              <section class="small-12 columns">
                <section class="panel">
                  <h5>Bantu!</h5>
                  <h6 class="subheader">Jawab Panggilan</h6>
                  <a href="#" class="small button" data-reveal-id="bayamModal">PILIH!</a>
                </section>
              </section>
            </section>
          </section>
        </section>

      </div>
    </section>