<head>
<<!--<script src="js/vendor/jquery-1.4.4.js"></script>
    <script type="text/javascript">
        $(function(){
            //initially hide the textbox
            $("#opkuliah").hide();
            $("#oppersonal").hide();
            $("#opwali").hide();
            $("#opppm").hide();

            $('#Tabel').change(function() {
              if($(this).find('option:selected').val() == "kuliah"){
                $("#opkuliah").show();
              }else{
                $("#opkuliah").hide();
              }
              if($(this).find('option:selected').val() == "personal"){
                $("#oppersonal").show();
              }else{
                $("#oppersonal").hide();
              }
              if($(this).find('option:selected').val() == "wali"){
                $("#opwali").show();
              }else{
                $('#opwali').hide();
              }
              if($(this).find('option:selected').val() == "ppm"){
                $("#opppm").show();
              }else{
                $("#opppm").hide();
              }
            });


            

        });
    </script>-->

</head>

<div class="row">
  <div class="small-8 columns">

  <div class="section-container tabs" data-section="tabs">
<!--Tabs2 content-->
    <section>
      <p class="title" data-section-title><a href="#">User Editor</a></p>
      <div class="content" data-section-content>
        <section class="row">
          <section class="small-6 columns">

           <fieldset>
            <legend>Cari Via Nomor Pendaftaran</legend>
            <form action="daftar/cek" method="post">
              <section class="small-10 columns">
                <br> <input type="text" name="daftar" placeholder="nomor pendaftaran ..." value="">
              </section>
              <section class="small-2 columns">
                <br> <input type="submit" class="small button" value="Cari">
              </section>
            </form> 
           </fieldset>
           <br/>


          <fieldset>
            <legend>Atau Via Nama Santri</legend>
            <form action="daftar/cek_by_name" method="post">
              <section class="small-10 columns">
                <br> <input type="text" name="Nama" placeholder="Nama Santri ..." value="">
              </section>
              <section class="small-2 columns">
                <br> <input type="submit" class="small button" value="Cari">
              </section>
            </form> 
           </fieldset>
           <br/>
            
            </section>

          <section class="small-6 columns">
            <section class="panel">
              <br><h4 class="hide-for-small">Edit User<hr/></h4>
              <h6 class="subheader">Ada masalah atau klaim mengenai data Santri ? edit disini dengan menggunakan <b>keyword</b> berupa nomor pendaftaran atau dapat menggunakan nama / nama panggilan santri </h6>
            </section>
            
          </section>
        </section>
      </div>
    </section>

     <section>
      <p class="title" data-section-title><a href="#">Find User</a></p>
      <div class="content" data-section-content>
        <section class="row">
          <section class="small-6 columns">
            <form action="search" method="post">
            <h4>Kategori</h4>                
              <select name="Tabel" id="Tabel" class="medium">
                <option>silahkan pilih kategori data</option>
                <option value="personal">Identitas Personal</option>
                <option value="kuliah">Perkuliahan</option>
                <option value="ppm">Tentang Kesantrian</option>
                <option value="wali">Perwalian</option>
              </select><br>

              <select name="Kolom1" id="opkuliah">
                <option value=""><b>silahkan pilih kategori keyword</b></option>
                <option value="daftar">Nomor Pendaftaran</option>
                <option value="univ">Nama Universitas</option>
                <option value="fak">Nama Fakultas</option>
                <option value="jur">Nama Jurusan</option>
                <option value="prodi">Nama Prodi</option>
                <option value="angkatan">Tahun Angkatan</option>
                <option value="alamat">Alamat Kampus</option>
                <option value="prestasi">Prestasi yang pernah dicapai</option>
              </select>

              <select name="Kolom2" id="oppersonal">
                <option value="">silahkan pilih kategori keyword</option>
                <option value="daftar">Nomor Pendaftaran</option>
                <option value="nama">Nama santri</option>
                <option value="panggilan">Nama Panggilan</option>
                <option value="namak">Asal kota/kabupaten</option>
                <option value="darah">Golongan Darah </option>
                <option value="hoby">Hobi</option>
              </select>

              <select name="Kolom4" id="opwali">
                <option value="">silahkan pilih kategori keyword</option>
                <option value="daftar">Nomor Pendaftaran</option>
                <option value="nama">Nama Orang Tua/ Wali</option>
                <option value="namako">Asal Kota/Kabupaten orang tua/ wali</option>
                <option value="dapukan">Dapukan Orang Tua/Wali</option>
                <option value="professi">Professi Orang Tua/Wali</option>
              </select>

              <select name="Kolom3" id="opppm">
                <option value="">silahkan pilih kategori keyword</option>
                <option value="daftar">Nomor Pendaftaran</option>
                <option value="dapukan">Dapukan sebelum masuk PPM</option>
                <option value="tahun">Angkatan PPM</option>
                <option value="kelompok">Asal Kelompok</option>
                <option value="desa">Asal Desa</option>
                <option value="daerah">Asal Daerah</option>
              </select>

           <br><br>
            <section class="small-10 columns">
            <input type="text" name="Keyword" placeholder="Keyword .. " value="">
            </section>
            <section class="small-2 columns">
            <input type="submit" class="small success radius button" value="Cari" >
            </section>  
            </form> 
          </section>
          <section class="small-6 columns">
            <section class="panel">
              <br><h4 class="hide-for-small">Find User<hr/></h4>
              <h6 class="subheader">ingin mencari data santri ? silahkan pilih kategori keyword yang akan anda masukkan terlebih dahulu apakah :<br>
                Data <b>Personal</b><br>
                Data <b>Perkuliahan</b><br>
                Data <b>Kesantrian</b><br>
                Data <b>Perwalian</b><br>
                Kemudian silahkan pilih kategori <b>keyword</b> yang lebih spesifik contoh: nama Santri,<br>
                Kemudian Ketikkan <b>keyword</b> pada kolom yang telah disediakan dan tekan tombol <b>Cari</b><br>
                Selamat Mencari, </h6>
            </section>
          </section>
        </section>
      </div>
    </section>



    <section>
      <p class="title" data-section-title><a href="#">Admin Dashboard</a></p>
      <div class="content" data-section-content>
        <section class="row">
          <section class="small-6 columns">
            <img src="http://placehold.it/500x500&text=Image"><br>
          </section>
          <section class="small-6 columns">
            <section class="panel">
              <br><h4 class="hide-for-small">Admin Page<hr/></h4>
              <p> ingin mengedit data admin mu ?<br/> atau ingin menambah admin ? <br>Disini tempatnya </p>
            </section>
            <section class="row">
              <section class="small-12 columns">
                <section class="panel">
                  <h5>Start</h5> <hr />
                  <section class="small-6 columns">
                  <h6 class="subheader">Tambah Admin</h6>
                  <a href="#" class="small button" data-reveal-id="bungaModal">PILIH!</a>
                  </section>
                  <section>
                  <h6 class="subheader">Edit Admin</h6>
                  <a href="#" class="small button" data-reveal-id="bungaModal">PILIH!</a>
                  </section>
                </section>
              </section>
            </section>
          </section>
        </section>
      </div>
    </section>

    <section>
      <p class="title" data-section-title><a href="#">About</a></p>
      <div class="content" data-section-content>
    <section>
      <section class="row">
        <section class="small-3 columns">
          <section class="small-12 columns">
            <img src="img/logo.png">
          </section>
          <section class="panel">
            <p>This Is some description about something really specific</p>
          </section>
        </section>
        <section class="small-9 columns">
          <section class="panel">
            <p>this is longer description that i want to talk about. lke you want to say about something long, but you don't know what to ssy , yeah i know
              that feeling because right now i have doing something like that. did you know that what i say is not going to be worth.
              so why are you still read it ?. is it make you confuse or angry ? i am just kidding :) so try to be nice to other people will make you
              get nice treat too . be good be nice be beautifull inside outside. oops i guess i just out of topics didn't i ? haha , keep on going bro! ganbatte! </p>
          </section>
        </section>
    </section>
    </section>
      </div>
    </section>

</div>
      
 
        <footer class="row">
        <div class="twelve columns"><hr />
            <div class="row">
 
              <div class="six columns">
                  <p>&copy; Copyright no one at all. Go to town.</p>
              </div> 
            </div>
        </div>
      </footer>
 
      <!-- End Footer -->
 
 
    </div>
  </div>

  <script type="text/javascript">
    if(!window.slider) var slider={};slider.data=[
    {"id":"slide-img-1","client":"Pilih Tanamanmu","desc":"Bayam"},
    {"id":"slide-img-2","client":"Pilih Tanamanmu","desc":"Bawang"},
    {"id":"slide-img-3","client":"Pilih Tanamanmu","desc":"Rumput"},
    {"id":"slide-img-4","client":"Pilih Tanamanmu","desc":"Sawi"},
    {"id":"slide-img-5","client":"Pilih Tanamanmu","desc":"Bbunga"},
    {"id":"slide-img-6","client":"Serif Addons","desc":"add your description here"},
    {"id":"slide-img-7","client":"Serif Addons","desc":"add your description here"}];
</script>



<script src="js/foundation/foundation.js"></script>
  
  <script src="js/foundation/foundation.dropdown.js"></script>
  
  <script src="js/foundation/foundation.placeholder.js"></script>
  
  <script src="js/foundation/foundation.forms.js"></script>
  
  <script src="js/foundation/foundation.alerts.js"></script>
  
  <script src="js/foundation/foundation.magellan.js"></script>
  
  <script src="js/foundation/foundation.reveal.js"></script>
  
  <script src="js/foundation/foundation.tooltips.js"></script>
  
  <script src="js/foundation/foundation.clearing.js"></script>
  
  <script src="js/foundation/foundation.cookie.js"></script>
  
  <script src="js/foundation/foundation.joyride.js"></script>
  
  <script src="js/foundation/foundation.orbit.js"></script>
  
  <script src="js/foundation/foundation.section.js"></script>
  
  <script src="js/foundation/foundation.topbar.js"></script>

  

<script src="js/foundation/foundation.orbit.js"></script>
<script src="js/foundation/foundation.alerts.js"></script>
<script src="js/foundation/foundation.js"></script>
  
  <!-- Included JS Files (Compressed) -->
  
  <script src="js/vendor/jquery.js"></script>
  <script src="js/foundation.min.js"></script>
  <script src="js/vendor/zepto.js"></script>
  
  <!-- Initialize JS Plugins -->
  <script src="javascripts/app.js"></script>

  <script>
    $(document).foundation();
  </script>

 <!-- <script type="text/javascript">
     $(window).load(function() {
         $('#slider').orbit();
     });
  </script>-->

  <script>
  document.write('<script src=/js/vendor/'
  +('__proto__' in {} ? 'zepto' : 'jquery')
  + '.js><\/script>');
  </script>