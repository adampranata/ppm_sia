<!doctype html>
<html lang="en">
<head>
    <!--<base href="<?php echo base_url(); ?>">-->
    <meta charset="UTF-8">
    <title><?php echo $title; ?> - PPM</title>
    <link rel="stylesheet" href="css/foundation.min.css" >
  <link rel="stylesheet" href="css/foundation.css" >
  <link rel="stylesheet" href="css/normalize.css" >
  <link rel="shortcut icon" href="img/favicon.ico"/>
  <link rel="stylesheet" type="text/css" href="css/demo.css" />
    <link rel="stylesheet" type="text/css" href="css/style1.css" />

 
  <script src="js/custom.modernizr.js"></script>
 
</head>
<body>  

        <div class="row">
            <div class="section-container">
            <div class="large-7 columns">
                <?php echo $content; ?> 
            </div>
        </div>
            <div class="large-5 columns">
                <?php echo $this->load->view('terms'); ?>
            </div>
                </div>
    
    <script src="js/jquery-1.8.2.min.js"></script>
    <script src="js/bootstrap.min.js"></script>

    <script src="js/foundation/foundation.js"></script>
  
  <script src="js/foundation/foundation.dropdown.js"></script>
  
  <script src="js/foundation/foundation.placeholder.js"></script>
  
  <script src="js/foundation/foundation.forms.js"></script>
  
  <script src="js/foundation/foundation.alerts.js"></script>
  
  <script src="js/foundation/foundation.magellan.js"></script>
  
  <script src="js/foundation/foundation.reveal.js"></script>
  
  <script src="js/foundation/foundation.tooltips.js"></script>
  
  <script src="js/foundation/foundation.clearing.js"></script>
  
  <script src="js/foundation/foundation.cookie.js"></script>
  
  <script src="js/foundation/foundation.joyride.js"></script>
  
  <script src="js/foundation/foundation.orbit.js"></script>
  
  <script src="js/foundation/foundation.section.js"></script>
  
  <script src="js/foundation/foundation.topbar.js"></script>

  

<script src="js/foundation/foundation.orbit.js"></script>
<script src="js/foundation/foundation.alerts.js"></script>
<script src="js/foundation/foundation.js"></script>
  
  <!-- Included JS Files (Compressed) -->
  
  <script src="js/vendor/jquery.js"></script>
  <script src="js/foundation.min.js"></script>
  <script src="js/vendor/zepto.js"></script>
  
  <!-- Initialize JS Plugins -->
  <script src="javascripts/app.js"></script>

  <script>
    $(document).foundation();
  </script>

 <!-- <script type="text/javascript">
     $(window).load(function() {
         $('#slider').orbit();
     });
  </script>-->

  <script>
  document.write('<script src=/js/vendor/'
  +('__proto__' in {} ? 'zepto' : 'jquery')
  + '.js><\/script>');
  </script>
</body>
</html>