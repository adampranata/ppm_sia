	<div class="large-12 columns <?php echo @$error; ?>">
		<small><?php echo @$pesan; ?></small>
	</div>

	<form action="daftar/daftarin" method="post">
    		<h3>Daftar!</h3>
				<div class="large-12 columns">
					<div class="formtext">
					<div class="row">
						<div class="small-3 columns <?php echo form_error('Daftar') ? 'error' : ''; ?>">
							Nomor Pendaftaran*
							<input type="number" name="Daftar" id="Daftar" min="1" max="9999" value="<?php echo set_value('Daftar'); ?>">
							<?php echo form_error('Daftar'); ?>
						</div>
					</div>

				<div class="row">
					<fieldset>
					<legend>Biodata</legend>
					<div class="row">

						<section class="small-5 columns <?php echo form_error('Nama') ? 'error' : ''; ?>">
							Nama Lengkap *<br>               
							<input type="text" name="Nama" value="<?php echo set_value('Nama') ?>">
							<?php echo form_error('Nama'); ?>
						</section>

						<section class="small-4 columns <?php echo form_error('Panggilan') ? 'error': ''; ?>">
							Nama Panggilan <br>
							<input type="text" name="Panggilan" value="<?php echo set_value('Panggilan'); ?>">
							<?php echo form_error('Panggilan'); ?>
						</section>
						<section class="small-3 columns <?php echo form_error('Kelamin') ? 'error' : ''; ?>">
							 Jenis Kelamin *<br>
							<input type="radio" name="Kelamin" value="L" <?php echo set_radio('Kelamin','L');?>/>L
							<input type="radio" name="Kelamin" value="P" <?php echo set_radio('Kelamin','P');?>/>P <br> <br>
							<?php echo form_error('Kelamin');?>
						</section>
					</div>

					<div class="row">
						<section class="small-4 columns">
							Kota Lahir<br>
							<input type="text" name="Kotalahir" value="<?php echo set_value('Kotalahir'); ?>">
						</section>
						<section class="small-4 columns <?php echo form_error('Tanggallahir') ? 'error' : '' ?>">
							Tanggal Lahir* <br>
							<input type="date" placeholder="yyyy-mm-dd" name="Tanggallahir" value="<?php echo set_value('Tanggallahir'); ?>">
							<?php echo form_error('Tanggallahir'); ?>
						</section>
						<section class="small-4 columns <?php echo form_error('Darah') ? 'error' : '' ?>">
							Golongan Darah* <br>
								<select name="Darah">
									<option selected="selected" value="<?php echo set_value('Darah'); ?>"><?php echo set_value('Darah'); ?></option>
									<option value="Belum">Belum tahu</option>
									<option value="A">A</option>
									<option value="B">B</option>
									<option value="AB">AB</option>
									<option value="O">O</option>
								</select>
								<?php echo form_error('Darah'); ?>
						</section>

				<section class="small-12 columns">
					Hobby<br>
					<input type="text" name="Hobby" value="<?php echo set_value('Hobby'); ?>">
				</section>
					</div>
			</fieldset></div><br />         

					<div class="row">							
					<fieldset> 
						<legend>Alamat Asal*</legend>

						<section class="row">
							<section class="small-6 columns <?php echo form_error('Kabkot') ? 'error' : ''; ?>">
								Kabupaten / Kota*
								<select name="Kabkot" class="Custom">
									<option selected ="selected" value="<?php echo set_value('Kabkot'); ?>"><?php echo set_value('Kabkot'); ?></option>
									<option value="Kabupaten">Kabupaten</option>
									<option value="Kota">Kota</option>
								</select><br>
								<?php echo form_error('Kabkot'); ?>
							</section>

							<section class="small-6 columns <?php echo form_error('Namak') ? 'error' : ''; ?>">
								Nama Kabupaten / Kota*
								<input type="text" name="Namak" value="<?php echo set_value('Namak'); ?>">	
								<?php echo form_error('Namak'); ?>
							</section>
						</section>

						<section class="row">

							<section class="small-6 columns <?php echo form_error('Kecamatan') ? 'error' : ''; ?>">
								Kecamatan* <br/>
								<input type = "text" name="Kecamatan" value="<?php echo set_value('Kecamatan')?>">
								<?php echo form_error('Kecamatan'); ?>
							</section>

							<section class="small-6 columns <?php echo form_error('Kelurahan') ? 'error' : ''; ?>">
								Kelurahan / Desa *
							<input type="text" name="Kelurahan" value="<?php echo set_value('Kelurahan'); ?>">
							<?php echo form_error('Kelurahan'); ?>
						</section>
					</section>

					<section class="row">
						<section class="small-9 columns <?php echo form_error('Alengkap') ? 'error' : '' ; ?>">
							Alamat Lengkap*<br>
							<input type="text" placeholder="contoh : Jl. D.I. Panjaitan no. 294, RT. 05 Rw. 03" name="Alengkap" value="<?php echo set_value('Alengkap'); ?>">
							<?php echo form_error('Alengkap');?>
						</section>

						<section class="small-3 columns">
							Kode Pos<br>
							<input type="text" name="Pos" value="<?php echo set_value('Pos'); ?>">
						</section>
						</fieldset>
					</div><br />

			<div class="row">
				<fieldset>
						<legend>Kontak</legend>
					
					<div class ="row">
						<section class="small-6 columns <?php echo form_error('Telp') ? 'error' : '' ?>">
							Nomor Telepon/HP *
							<input type="tel" name="Telp" value="<?php echo set_value('Telp'); ?>">
							<?php echo form_error('Telp');?>
						</section>

						<section class="small-6 columns">
							E-mail
							<input type="text" name="Email" value="<?php echo set_value('Email'); ?>">
						</section>
					</div>

					<div class="row">
						<section class="small-6 columns">
							Facebook
							<input type="text" name="Facebook" value="<?php echo set_value('Facebook'); ?>">
						</section>

						<section class="small-6 columns">
							Twitter
							<input type="text" name="Twitter" value="<?php echo set_value('Twitter'); ?>">
						</section>
					</div>

				</fieldset><br />
			</div>


					<input type="hidden" name="time" value="<?php echo date("Y-d-m h:m:s");?>">
					<input type="hidden" name="tahap_1" value="sudah">
					<input type="hidden" name="status" value='2'>
					<input type="submit" value="Daftar" class="button radius left">
			</div>
			</div>
    </form>
