<form action="daftar/daftarin2" method="post" id= "form_id">
<div class="formtext">
    <legend><h3 >Daftar!</h3></legend>
    			<div class="small-12 columns">
					<div class="row">
						<section class="small-4 columns">
							Nomor Pendaftaran*<br>
							<?php echo $cek2->daftar; ?>
						</section>
					</div>

			<div class="row">

				<section class="small-7 columns">
					UNIVERSITAS*<br>                
					<input id="Other" name="Other" type="text" placeholder="Other" value="<?php echo $cek2->univ;?>"> 
					
				</section>

				<section class="small-5 columns">
					FAKULTAS*<br>
					<input type="text" name="Fakultas" value="<?php echo $cek2->fak; ?>">
				</section>
			</div>

			<div class="row">
				<section class="small-5 columns">
					JURUSAN<br>
					<input type="text" name="Jurusan" value="<?php echo $cek2->jur; ?>"><br><br>
				</section>
				<section class="small-4 columns">
					PRODI<br>
					<input type="text" name="Prodi" value="<?php echo $cek2->prodi; ?>"><br><br>
				</section>
				<section class="small-3 columns">
					ANGKATAN*<br>
					<input type="number" name="Angkatan" min="1" max="9999" value="<?php echo $cek2->angkatan; ?>">
					
				</section>
			</div>

			<div class="row">
				<section class="small-12 columns">
					Alamat Kampus<br>
					<input type="text" name="Alamat" value="<?php echo $cek2->alamat; ?> "><br><br>
				</section>
			</div>
			<div class="row">
				<section class="small-12 columns">
				Prestasi Yang Pernah Dicapai<br>
					<input type="text" name="Prestasi" value="<?php echo $cek2->prestasi; ?>"><br><br>
				</section>
			</div>
		</div>

		<input type="hidden" name="time" value="<?php echo date("Y-d-m h:m:s");?>">
		<input type="hidden" name="tahap_2" value="sudah">
		<input type="hidden" name="status" value='3'>
		<div><input type="submit" value="Daftar" class="button radius right"></div>
</div>
</form>
