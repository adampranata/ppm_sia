		<form action="daftar/daftarin" method="post">
    		<h3>Let's Be Part Of PPM IT Community</h3>
				<div class="large-12 columns">
					<div class="formtext">
					<div class="row">
					<fieldset>
					<legend>Biodata</legend>
					<div class="row">

						<section class="small-5 columns <?php echo form_error('Nama') ? 'error' : ''; ?>">
							Nama Lengkap *<br>               
							<input type="text" name="Nama" value="<?php echo set_value('Nama') ?>">
							<?php echo form_error('Nama'); ?>
						</section>

						<section class="small-4 columns <?php echo form_error('Panggilan') ? 'error': ''; ?>">
							Nama Panggilan <br>
							<input type="text" name="Panggilan" value="<?php echo set_value('Panggilan'); ?>">
							<?php echo form_error('Panggilan'); ?>
						</section>
						<section class="small-3 columns <?php echo form_error('Kelamin') ? 'error' : ''; ?>">
							 Jenis Kelamin *<br>
							<input type="radio" name="Kelamin" value="L" <?php echo set_radio('Kelamin','L');?>/>L
							<input type="radio" name="Kelamin" value="P" <?php echo set_radio('Kelamin','P');?>/>P <br> <br>
							<?php echo form_error('Kelamin');?>
						</section>
					</div>

				
				<fieldset>
						<legend>Kontak</legend>
					
					<div class ="row">
						<section class="small-6 columns <?php echo form_error('Telp') ? 'error' : '' ?>">
							Nomor Telepon/HP *
							<input type="tel" name="Telp" value="<?php echo set_value('Telp'); ?>">
							<?php echo form_error('Telp');?>
						</section>

						<section class="small-6 columns">
							E-mail
							<input type="text" name="Email" value="<?php echo set_value('Email'); ?>">
						</section>
					</div>

					<div class="row">
						<section class="small-6 columns">
							Facebook
							<input type="text" name="Facebook" value="<?php echo set_value('Facebook'); ?>">
						</section>

						<section class="small-6 columns">
							Twitter
							<input type="text" name="Twitter" value="<?php echo set_value('Twitter'); ?>">
						</section>
					</div>

				</fieldset><br />
			</div>


					<input type="submit" value="Daftar" class="button radius left">
			</div>
			</div>
    </form>
