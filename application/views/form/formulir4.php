	<form action="daftar/daftarin4" method="post" id= "form_id">
		<div class="small-12 columns <?php echo @$error; ?>">
		<small><?php echo @$pesan; ?></small>
</div>
    <legend><h3 class="form-signin-heading">Daftar!</h3></legend>
			<div class="small-12 columns">
				<div class="formtext">

				<div class="row">
					<section class="small-4 columns <?php echo form_error('Daftar') ? 'error' : '' ?>">
						Nomor Pendaftaran* <br>
						<input type="number" name="Daftar" min="1" max="9999" value="<?php echo set_value('Daftar'); ?>">
						<?php echo form_error('Daftar'); ?>
					</section>
				</div>

				<div class="row">
					<fieldset>
						<legend>Biodata Orang Tua/ Wali*</legend>

				<div class="row">
					<section class="small-12 columns">
					<section class="small-6 columns <?php echo form_error('Jenis') ? 'error' : '' ?>">
						Identitas Orang tua / Wali * <br>
							<select name="Jenis">
								<option value="<?php echo set_value('Jenis'); ?>"><?php echo set_value('Jenis'); ?></option> 
								<option value="Ayah">Ayah</option>
								<option value="Ibu">Ibu</option>
								<option value="Wali">Wali</option>
							</select>
							<?php echo form_error('Jenis'); ?>		
					</section>

					<section class="small-6 columns <?php echo form_error('Nama') ? 'error' : '' ?>">
						Nama Orang Tua / Wali * <br/>
						<input type="text" name="Nama" value="<?php echo set_value('Nama'); ?>">
						<?php echo form_error('Nama'); ?>
					</section>

				</section>
				</div>


				<div class="row">
					<section class="small-6 columns">
						Dapukan Orang Tua / Wali <br>
						<input type="text" name="Dapukan" value="<?php echo set_value('Dapukan'); ?>">
						Email Orang Tua / Wali <br/>
						<input type="text" name="Email" value="<?php echo set_value('Email'); ?>">
					</section>
					
					<section class="small-6 columns">
						Professi Orang Tua / Wali <br>
						<input type="text" name="Professi" value="<?php echo set_value('Professi'); ?>"><br>
					</section>
				</div>

				<div class="row">
					<section class="small-12 columns">
					<section class="small-6 columns <?php echo form_error('Status') ? 'error' : '' ?>">
						Status Orang Tua / Wali * <br />
						<input type="checkbox" name="Status" value="jamaah" <?php echo set_checkbox('Status','jamaah'); ?>/>Jama'ah
						<input type="checkbox" name="Status" value="belum" <?php echo set_checkbox('Status', 'belum'); ?>/>Belum
						<?php echo form_error('Status') ?>
					</section>

					<section class="small-6 columns <?php echo form_error('Telp') ? 'error' : '' ?>">
						Nomor Telpon Orang Tua / Wali * <br/>
							<input type="number" name="Telp" value="<?php echo set_value('Telp'); ?>">
							<?php echo form_error('Telp'); ?>
					</section>
					</section>
				</div>
					</fieldset>
				</div><br / >

				<!--<div class="row">
					<section class="small-8 columns <?php echo form_error('Ibu') ? 'error' : '' ?>">
						<p>Nama Ibu* <br>
						<input type="text" name="Ibu" value="<?php echo set_value('Ibu'); ?>">
						<?php echo form_error('ibu'); ?></p>
					</section>
					<section class="small-4 columns <?php echo form_error('Statusi') ? 'error' : '' ?>">
						<p>Status Ibu* <br>
						<input type="checkbox" name="Statusi" value="jamaah" <?php echo set_checkbox('Statusi','jamaah'); ?> />Jama'ah
						<input type="checkbox" name="Statusi" value="belum" <?php echo set_checkbox('Statusi', 'belum'); ?> />Belum<br>

						<?php echo form_error('Statusi'); ?> </p><br>
					</section>
				</div>-->


				<div class="row">							
					<fieldset> 
						<legend>Alamat Orang Tua/Wali*</legend>

						<section class="row">
							<section class="small-6 columns">
								Kabupaten / Kota*
								<select name="Kabkoto" class="Custom">
									<option selected ="selected" value="<?php echo set_value('Kabkoto'); ?>"><?php echo set_value('Kabkoto'); ?></option>
									<option value="Kabupaten">Kabupaten</option>
									<option value="Kota">Kota</option>
								</select><br>
							</section>

							<section class="small-6 columns">
								Nama Kabupaten / Kota*
								<input type="text" name="Namako" value="<?php echo set_value('Namako'); ?>">	
							</section>
						</section>

						<section class="row">

							<section class="small-6 columns">
								Kecamatan* <br/>
								<input type = "text" name="Kecamatano" value="<?php echo set_value('Kecamatano')?>">
							</section>

							<section class="small-6 columns">
								Kelurahan / Desa *
							<input type="text" name="Kelurahano" value="<?php echo set_value('Kelurahano'); ?>">
							<?php echo form_error('Kelurahano');?>
						</section>
					</section>

					<section class="row">
						<section class="small-9 columns">
							Alamat Lengkap*<br>
							<input type="text" placeholder="contoh : Jl. D.I. Panjaitan no. 294, RT. 05 Rw. 03" name="Alengkapo" value="<?php echo set_value('Alengkapo'); ?>">
						</section>

						<section class="small-3 columns">
							Kode Pos<br>
							<input type="text" name="Pos" value="<?php echo set_value('Pos'); ?>">
						</section>
						</fieldset>
					</div><br />


					<div class="row">							
					<fieldset> 
						<legend>Data Pengurus Asal*</legend>

						<section class="row">
							

							<section class="small-4 columns">
								Nama Pengurus Asal 
								<input type="text" name="Namap" value="<?php echo set_value('Namap'); ?>">	
							</section>
						
							<section class="small-4 columns">
								Dapukan <br/>
								<input type = "text" name="Dapukanp" value="<?php echo set_value('Kecamatano')?>">
							</section>

							<section class="small-4 columns">
								Nomor telepon
							<input type="text" name="Telpp" value="<?php echo set_value('Telpp'); ?>">
							<?php echo form_error('Asal');?>
						</section>

						</section>

						</section>
						</fieldset>
					</div><br />
				
				<!--<section class="row">
					<section class="small-6 columns">
						Nomor Telpon Ibu<br>
						<input type="text" name="Telpi"><br><br>
					</section>
					<section class="small-6 columns">
						Email Ibu<br>
						<input type="text" name="Emaili" value=""><br><br>
					</section>
				</section>

				<section class="row">
					<section class="small-6 columns">
						Profesi Ayah<br>
						<input type="text" name="Kerjaayah" value=""><br><br>
					</section>
					<section class="small-6 columns">
						Dapukan Ayah<br>
						<input type="text" name="Dapukana" value=""><br><br>
					</section>
				</section>

				<section class="row">
					<section class="small-6 columns">
						Profesi Ibu<br>
						<input type="text" name="Kerjaibu" value=""><br><br>
					</section>
				<input type="hidden" name="tahap_4" value="sudah">
				<input type="hidden" name="status" value=''>
				</section>-->

				<!--<section class="row">
					<section class="small-4 columns">
						<p>Kelompok<br>
						<input type="text" name="Kelompok" value=""><br></p><br>
					</section>

					<section class="small-4 columns">
						<p>Desa<br>
						<input type="text" name="Desa" value=""><br></p><br>
					</section>

					<section class="small-4 columns">
						<p>Daerah<br>
						<input type="text" name="Daerah" value=""><br></p><br>
						<input type="hidden" name="tahap_4" value="sudah">
					</section>
				</section>-->
				<input type="hidden" name="tahap_4" value="sudah">
				<input type="hidden" name="status" value='5'>
		<div><input type="submit" value="Daftar" class="button radius right"></div>
	</div>
</div>
</form>
