	<div class="small-12 columns <?php echo @$error; ?>">
		<small><?php echo @$pesan; ?></small>
	</div>

	<form action="daftar/edita" method="post">
		<fieldset>
    		<legend ><h3>Daftar!</h3></legend>
				<div class="small-12 columns">
					<div class="row">
						<div class="small-3 columns <?php echo form_error('Daftar') ? 'error' : ''; ?>">
							<p>Nomor Pendaftaran* <br/>
							<?php echo $tahap1->daftar; ?>
							<?php echo form_error('Daftar'); ?>
							<input type="hidden" name="Daftar" value="<?php echo $tahap1->daftar; ?>">
						</div>
					</div>

					<div class="row">
						<section class="small-5 columns <?php echo form_error('Nama') ? 'error' : ''; ?>">
							Nama Lengkap *<br>               
							<input type="text" name="Nama" value="<?php echo $tahap1->nama; ?>">
							<?php echo form_error('Nama'); ?>
						</section>

						<section class="small-4 columns <?php echo form_error('Panggilan') ? 'error': ''; ?>">
							<p>Nama Panggilan <br>
							<input type="text" name="Panggilan" value="<?php echo $tahap1->panggilan; ?>">
							<?php echo form_error('Panggilan'); ?></p>
						</section>

						<section class="small-3 columns <?php echo form_error('Kelamin') ? 'error' : ''; ?>">
							<p> Jenis Kelamin *<br>
							<select name='Kelamin'>
								<option selected="selected" value="<?php echo $tahap1->kelamin; ?>"><?php echo $tahap1->kelamin; ?></option>
								<option value="L">L</option>
								<option value="P">P</option>
							</select>
							<?php echo form_error('Kelamin');?>
						</section>
					</div>

					<div class="row">
						<section class="small-5 columns">
							<p>Kota Lahir<br>
							<input type="text" name="Kota" value="<?php echo $tahap1->kotalahir ; ?>"><br></p>
						</section>
						<section class="small-4 columns <?php echo form_error('Tanggal') ? 'error' : '' ?>">
							<p>Tanggal Lahir* <br>
							<input type="date" name="Tanggal" value="<?php echo $tahap1->tanggallahir; ?>">
							<?php echo form_error('Tanggal'); ?> </p>
						</section>
						<section class="small-3 columns <?php echo form_error('Darah') ? 'error' : '' ?>">
							<p>Golongan Darah* <br>
								<select name="Darah">
									<option value="<?php echo $tahap1->darah; ?>"><?php echo $tahap1->darah; ?></option>
									<option value="A">A</option>
									<option value="B">B</option>
									<option value="AB">AB</option>
									<option value="O">O</option>
								</select>
								<?php echo form_error('Darah'); ?> </p>
						</section>
					</div>

					<div class="row">
						<section class="small-9 columns <?php echo form_error('Asal') ? 'error' : '' ;?>">
							<p>Alamat Asal*<br>
							<input type="text" name="Asal" value="<?php echo $tahap1->asal; ?>">
							<?php echo form_error('Asal');?> </p>
						</section>

						<section class="small-3 columns">
							<p>Kode Pos<br>
							<input type="text" name="Pos" value="<?php echo $tahap1->pos; ?>"><br></p><br>
						</section>
					</div>

					<div class ="row">

						<section class="small-6 columns <?php echo form_error('Telp') ? 'error' : '' ?>">
							<p>Nomor Telepon/HP *
							<input type="tel" name="Telp" value="<?php echo $tahap1->telp; ?>">
							<?php echo form_error('Telp');?> </p>
						</section>

						<section class="small-6 columns">
							<p>E-mail<br>
							<input type="text" name="Email" value="<?php echo $tahap1->email; ?>"><br></p><br>
						</section>
					</div>

					<div class="row">
						<section class="small-6 columns">
							<p>Facebook<br>
							<input type="text" name="Facebook" value="<?php echo $tahap1->facebook; ?>"><br></p><br>
						</section>

						<section class="small-6 columns">
							<p>Twitter<br>
							<input type="text" name="Twitter" value="<?php echo $tahap1->twitter; ?>"><br></p><br>
						</section>
					</div>

					<p>Hobby<br>
					<input type="text" name="Hobby" value="<?php echo $tahap1->hoby; ?>"><br></p><br>

					<input type="hidden" name="time" value="<?php echo date("Y-d-m h:m:s");?>">
					<input type="hidden" name="tahap1" value="sudah">
					<input type="hidden" name="status" value='2'>
					<input type="submit" value="Edit" class="button radius left">
				</div>
        </fieldset>
    </form><br><br>
