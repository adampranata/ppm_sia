<?php echo $daftar; ?>
	<form action="daftar/editlast" method="post">
    		<legend ><h3>Edit</h3></legend>
				<div class="large-12 columns">
					<div class="formtext">
					<div class="row">
						<div class="large-3 columns <?php echo form_error('Daftar') ? 'error' : ''; ?>">
							Nomor Pendaftaran*
							<?php echo $cek1->daftar; ?>
							<input type="hidden" name="Daftar" id="Daftar" value="<?php echo $cek1->daftar; ?>">
						</div>
					</div>

					<div class="row">
					<fieldset>
					<legend>Biodata</legend>
					<div class="row">

						<section class="small-5 columns <?php echo form_error('Nama') ? 'error' : ''; ?>">
							Nama Lengkap *<br>               
							<input type="text" name="Nama" value="<?php echo $cek1->nama; ?>">
						</section>

						<section class="small-4 columns <?php echo form_error('Panggilan') ? 'error': ''; ?>">
							Nama Panggilan <br>
							<input type="text" name="Panggilan" value="<?php echo $cek1->panggilan; ?>">
						</section>
						<section class="small-3 columns <?php echo form_error('Kelamin') ? 'error' : ''; ?>">
							 Jenis Kelamin *<br>
							<select name='Kelamin'>
								<option selected="selected" value="<?php echo $cek1->kelamin; ?>"><?php echo $cek1->kelamin; ?></option>
								<option value="L">L</option>
								<option value="P">P</option>
							</select>
						</section>
					</div>

					<div class="row">
						<section class="small-4 columns">
							Kota Lahir<br>
							<input type="text" name="Kota" value="<?php echo $cek1->kotalahir; ?>"><br>
						</section>
						<section class="small-4 columns <?php echo form_error('Tanggal') ? 'error' : '' ?>">
							Tanggal Lahir* <br>
							<input type="date" name="Tanggal" value="<?php echo $cek1->tanggallahir; ?>">
						</section>
						<section class="small-4 columns <?php echo form_error('Darah') ? 'error' : '' ?>">
							Golongan Darah* <br>
								<select name="Darah">
									<option selected="selected" value="<?php echo $cek1->darah; ?>"><?php echo $cek1->darah; ?></option>
									<option value="A">A</option>
									<option value="B">B</option>
									<option value="AB">AB</option>
									<option value="O">O</option>
								</select>
						</section>
						<section class="small-12 columns">
					Hobby<br>
					<input type="text" name="Hobby" value="<?php echo $cek1->hoby; ?>"><br><br>
						</section>
					</div>
						</fieldset>
					</div>

					<div class="row">
						<section class="large-9 columns <?php echo form_error('Asal') ? 'error' : '' ;?>">
							Alamat Asal*<br>
							<input type="text" name="Asal" value="<?php echo $cek1->asal; ?>">
						</section>

						<section class="large-3 columns">
							Kode Pos<br>
							<input type="text" name="Pos" value="<?php echo $cek1->pos; ?>"><br><br>
						</section>
					</div>

					<div class ="row">

						<section class="large-6 columns <?php echo form_error('Telp') ? 'error' : '' ?>">
							Nomor Telepon/HP *
							<input type="tel" name="Telp" value="<?php echo $cek1->telp; ?>">
						</section>

						<section class="large-6 columns">
							E-mail<br>
							<input type="text" name="Email" value="<?php echo $cek1->email; ?>"><br><br>
						</section>
					</div>

					<div class="row">
						<section class="small-6 columns">
							Facebook<br>
							<input type="text" name="Facebook" value="<?php echo $cek1->facebook; ?>"><br><br>
						</section>

						<section class="small-6 columns">
							Twitter<br>
							<input type="text" name="Twitter" value="<?php echo $cek1->twitter; ?>"><br><br>
						</section>
					</div>


					<input type="hidden" name="time" value="<?php echo date("Y-d-m h:m:s");?>">
					<input type="hidden" name="tahap_1" value="sudah">
					<input type="hidden" name="daftar" value="<?php echo $daftar; ?>">
					<input type="hidden" name="status" value='2'>
					<input type="submit" value="Edit" class="button radius">
				</form>

					<form action="daftar/cek" method="post" >

					<input type="hidden" value="<?php echo $daftar; ?>" name="daftar">
					<input type="submit" value="Lanjut" class="button radius">
					</form>
			</div>
			</div>
  <br><br>
