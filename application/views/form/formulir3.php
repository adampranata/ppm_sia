<head>

 <script src="js/vendor/jquery-1.4.4.js"></script>
    <script type="text/javascript">
        $(function(){
            //initially hide the textbox
            $("#Tahun").hide();
            $('#Statusj').change(function() {
              if($(this).find('option:selected').val() == "Tahun"){
                $("#Tahun").show();
              }else{
                $("#Tahun").hide();
              }
            });
        });
    </script>


     <script src="js/vendor/jquery-1.4.4.js"></script>
    <script type="text/javascript">
        $(function(){
            //initially hide the textbox
            $("#Mubaligh").hide();
            $('#Statusm').change(function() {
              if($(this).find('option:selected').val() == "Mubaligh"){
                $("#Mubaligh").show();
              }else{
                $("#Mubaligh").hide();
              }
            });
        });
    </script>

    <script src="js/vendor/jquery-1.4.4.js"></script>
    <script type="text/javascript">
        $(function(){
            //initially hide the textbox
            $("#Pernah").hide();
            $('#Statust').change(function() {
              if($(this).find('option:selected').val() == "Pernah"){
                $("#Pernah").show();
              }else{
                $("#Pernah").hide();
              }
            });
        });
    </script>

</head>

<div class="large-12 columns <?php echo @$error; ?>">
		<small><?php echo @$pesan; ?></small>
</div>

	<form action="daftar/daftarin3" method="post" id= "form_id">
	<legend><h3>Daftar!</h3></legend>
			<div class="small-12 columns">
				<div class="formtext">
				<div class="row">
					<section class="small-4 columns <?php echo form_error('Daftar') ? 'error' : '' ?>">
						Nomor Pendaftaran*<br>
						<input type="number" name="Daftar" min="1" max="9999" value="<?php echo set_value('Daftar'); ?>">
						<?php echo form_error('Daftar'); ?>
					</section>
				</div>

				<div class="row">
					<section class="small-6 columns">
						Bulan Masuk PPM  <br>                
							<select name="Bulan">
								<option value="<?php echo set_value('Bulan')?>"><?php echo set_value('Bulan'); ?></option>
								<option value="Januari">Januari</option>
								<option value="Februari">Februari</option>
								<option value="Maret">Maret</option>
								<option value="April">April</option>
								<option value="Mei">Mei</option>
								<option value="Juni">Juni</option>
								<option value="Juli">Juli</option>
								<option value="Agustus">Agustus</option>
								<option value="September">September</option>
								<option value="Oktober">Oktober</option>
								<option value="November">November</option>
								<option value="Desember">Desember</option>
							</select><br>
					</section>
					<section class="small-6 columns <?php echo form_error('Tahun') ? 'error' : '' ; ?>">
						Tahun Masuk*<br>
						<input type="number" min="1" max="9999" name="Tahun" value="<?php echo set_value('Tahun'); ?>">
						<?php echo form_error('Tahun'); ?>
					</section>
				</div>

				<div class="row">
					<section class="small-6 columns">
						Dapukan Sebelum Masuk PPM <br>
						<input type="text" name="Dapukan" value="<?php echo set_value('Dapukan'); ?>">
					</section>
					<section class="small-6 columns <?php echo form_error('Statusj') ? 'error' : '' ; ?>">
						Masuk Jamaah*<br>
							<select name="Statusj" id="Statusj" >
								<option value="<?php echo set_value('Statusj'); ?>"><?php echo set_value('Statusj');?></option>
								<option value="Lahir">Lahir</option>
								<option value="Tahun">Tahun</option>
							</select>
						<input type="text" name="Tahunj" id="Tahun" value="<?php echo set_value('Tahunj'); ?>" placeholder="Tahun Jamaah" >
						<?php echo form_error('Statusj'); ?>
					</section>
			</div>

			<div class="row">

				<section class="small-6 columns <?php echo form_error('Statusm') ? 'error' : '' ; ?>">
						Status Mubaligh*<br>
							<select name="Statusm" id="Statusm" >
								<option value="<?php echo set_value('Statusm'); ?>"><?php echo set_value('Statusm');?></option>
								<option value="Belum">Belum</option>
								<option value="Mubaligh">Mubaligh</option>
							</select>
						<input type="text" name="Tahunm" id="Mubaligh" value="<?php echo set_value('Tahunm'); ?>" placeholder="Tahun Mubligh" >
						<?php echo form_error('Statusm'); ?>
					</section>
			
			<section class="small-6 columns <?php echo form_error('Statust') ? 'error' : '' ; ?>">
						Status Tugas<br>
							<select name="Statust" id="Statust" >
								<option value="<?php echo set_value('Statust'); ?>"><?php echo set_value('Statust');?></option>
								<option value="Belum">Belum Pernah Tugas</option>
								<option value="Pernah">Pernah Tugas</option>
							</select>
						<input type="text" name="Tugas" id="Pernah" value="<?php echo set_value('Tugas'); ?>" placeholder="Kota Tugas ... " >
					</section>
			</div>



			<div class="row">
					<h5>Asal*</h5>
				<section class="small-4 columns <?php echo form_error('Kelompok') ? 'error' : '' ?>">
					Kelompok*<br>
					<input type="text" name="Kelompok" value="<?php echo set_value('Kelompok'); ?>">
					<?php echo form_error('Kelompok'); ?>
				</section>

				<section class="small-4 columns <?php echo form_error('Desa') ? 'error' : '' ;?>">
					Desa*<br>
					<input type="text" name="Desa" value="<?php echo set_value('Desa') ;?>">
					<?php echo form_error('Desa'); ?>
				</section>
				<section class="small-4 columns <?php echo form_error('Daerah') ? 'error' : '' ;?>"> 
					Daerah*<br>
					<input type="text" name="Daerah" value="<?php echo set_value('Daerah'); ?>">
					<?php echo form_error('Daerah'); ?>
					<input type="hidden" name="time" value="<?php echo date("Y-d-m h:m:s");?>">
					
				<input type="hidden" name="tahap_3" value="sudah">
				<input type="hidden" name="status" value='4'>
				</section>
			</div>
		</div>
		</div>
		<div><input type="submit" value="Daftar" class="button radius right"></div></form>