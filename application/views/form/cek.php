<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8" />
  <base href="<?php echo base_url(); ?>">
  <!-- Set the viewport width to device width for mobile -->
  <meta name="viewport" content="width=device-width" />

  <title><?php echo $title; ?> - PPM </title>
  <!-- Included CSS Files (Uncompressed)
  <link rel="stylesheet" href="stylesheets/foundation.css">
  -->
  
  <!-- Included CSS Files (Compressed) -->
  <link rel="stylesheet" href="css/foundation.min.css" >
  <link rel="stylesheet" href="css/foundation.css" >
  <link rel="stylesheet" href="css/normalize.css" >
  <link rel="shortcut icon" href="img/favicon.ico"/>
  <link rel="stylesheet" type="text/css" href="css/demo.css" />
  <link rel="stylesheet" type="text/css" href="css/style1.css" />
  

  <!--<link rel="stylesheet" href="stylesheets/app.css">-->
  <script src="js/custom.modernizr.js"></script>
    <link rel="shortcut icon" href="../favicon.ico"> 
                <link href='http://fonts.googleapis.com/css?family=Terminal+Dosis' rel='stylesheet' type='text/css' />

</head>
<body>  
  <div class="row">
     <div class="small-12 columns">
        
      <div class="small-9 columns">
        <div class="small-12 columns">
          <?php 

          $data['cek1'] = $this->profile_m->get_personal($daftar);

          $this->load->view('/cek/'.$tahap1, $data);?>
        </div><br><br>

        <div class="small-12 columns">
        <?php 
          $data['cek2'] = $this->profile_m->get_kuliah($daftar);

          $this->load->view('/cek/'.$tahap2, $data); ?>

        </div><br><br>

        <div class="small-12 columns">
          <?php 
          $data['cek3'] = $this->profile_m->get_ppm($daftar); 

          $this->load->view('cek/'. $tahap3, $data);
          ?>
        </div><br><br>

        <div class="small-12 columns">
          <?php 
          $data['cek4'] = $this->profile_m->get_wali($daftar);

          $this->load->view('cek/'.$tahap4, $data); 
        ?>
        </div>
      </div>
    </div>
  </div>

    <script src="js/foundation/foundation.js"></script>
  
  <script src="js/foundation/foundation.dropdown.js"></script>
  
  <script src="js/foundation/foundation.placeholder.js"></script>
  
  <script src="js/foundation/foundation.forms.js"></script>
  
  <script src="js/foundation/foundation.alerts.js"></script>
  
  <script src="js/foundation/foundation.magellan.js"></script>
  
  <script src="js/foundation/foundation.reveal.js"></script>
  
  <script src="js/foundation/foundation.tooltips.js"></script>
  
  <script src="js/foundation/foundation.clearing.js"></script>
  
  <script src="js/foundation/foundation.cookie.js"></script>
  
  <script src="js/foundation/foundation.joyride.js"></script>
  
  <script src="js/foundation/foundation.orbit.js"></script>
  
  <script src="js/foundation/foundation.section.js"></script>
  
  <script src="js/foundation/foundation.topbar.js"></script>

  

<script src="js/foundation/foundation.orbit.js"></script>
<script src="js/foundation/foundation.alerts.js"></script>
<script src="js/foundation/foundation.js"></script>
  
  <!-- Included JS Files (Compressed) -->
  
  <script src="js/vendor/jquery.js"></script>
  <script src="js/foundation.min.js"></script>
  <script src="js/vendor/zepto.js"></script>
  
  <!-- Initialize JS Plugins -->
  <script src="javascripts/app.js"></script>

  <script>
    $(document).foundation();
  </script>
  <script>
  document.write('<script src=/js/vendor/'
  +('__proto__' in {} ? 'zepto' : 'jquery')
  + '.js><\/script>');
  </script>
</body>
</html>