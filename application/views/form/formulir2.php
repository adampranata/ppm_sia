 <head>
 <script src="js/vendor/jquery-1.4.4.js"></script>
    <script type="text/javascript">
        $(function(){
            //initially hide the textbox
            $("#Other").hide();
            $('#Univ').change(function() {
              if($(this).find('option:selected').val() == "Other"){
                $("#Other").show();
              }else{
                $("#Other").hide();
              }
            });
            $("#Other").keyup(function(ev){
                var othersOption = $('#Univ').find('option:selected');
                if(othersOption.val() == "Other")
                {
                    ev.preventDefault();
                    //change the selected drop down text
                    $(othersOption).html($("#Other").val()); 
                } 
            });
            $('#form_id').submit(function() {
                var othersOption = $('#Univ').find('option:selected');
                if(othersOption.val() == "Other")
                {
                    // replace select value with text field value
                    othersOption.val($("#Other").val());
                }
            });
        });
    </script>
</head>


<div class="large-12 columns <?php echo @$error; ?>">
		<small><?php echo @$pesan; ?></small>
</div>

<form action="daftar/daftarin2" method="post" id= "form_id">
<div class="formtext">
    <legend><h3 >Daftar!</h3></legend>
    			<div class="small-12 columns">
					<div class="row">
						<section class="small-4 columns <?php echo form_error('Daftar') ? 'error' : '' ;?>">
							Nomor Pendaftaran*<br>
							<input type="number" name="Daftar" min="1" max="9999" value ="<?php echo set_value('Daftar'); ?>">
							<?php echo form_error('Daftar'); ?>
						</section>
					</div>

			<div class="row">

				<section class="small-7 columns <?php echo form_error('Univ') ? 'error' : '' ;?>">
					UNIVERSITAS*<br>                
					<select id='Univ' name='Univ'>
						<option value="<?php echo set_value('Univ'); ?>"><?php echo set_value('Univ'); ?> </option>
						<option value="Universitas Gadjah Mada">Universitas Gadjah Mada</option>
						<option value="Universitas Negeri Yogyakarta">Universitas Negeri Yogyakarta</option>
						<option value="Universitas Islam Negeri Sunan Kalijaga">Universitas Islam Negeri Sunan Kalijaga</option>
						<option value="Universitas Ahmad Dahlan">Universitas Ahmad Dahlan</option>
						<option value="Universitas Cokroaminoto">Universitas Cokroaminoto</option>
						<option value="Universitas Islam Indonesia">Universitas Islam Indonesia</option>
						<option value="Universitas Muhammadiyah Yogyakarta">Universitas Muhammadiyah Yogyakarta</option>
						<option value="Universitas Pembangunan Nasional Veteran">Universitas Pembangunan Nasional Veteran</option>
						<option value="Universitas Teknologi Yogyakarta">Universitas Teknologi Yogyakarta</option>
						<option value="Universitas Sarjanawiyata Tamansiswa">Universitas Sarjanawiyata Tamansiswa</option>
						<option value="Institut Sains dan Teknologi Akprind">Institut Sains dan Teknologi Akprind</option>
						<option value="Lembaga Pendidikan Perkebunan">Lembaga Pendidikan Perkebunan (LPP)</option>
						<option value="Sekolah Tinggi Teknologi Nasional">Sekolah Tinggi Teknologi Nasional</option>
						<option value="STMIK Akakom">STMIK Akakom</option>
						<option value="STMIK AMIKOM">STMIK AMIKOM</option>
						<option value="STIE YKPN">STIE YKPN</option>
						<option value="Institut Pertanian Stiper">Institut Pertanian Stiper</option>
						<option value="Akademi Kebidanan Yogyakarta">Akademi Kebidanan Yogyakarta</option>
						<option value="Akademi Keperawatan Al Islam Yogyakarta">Akademi Keperawatan Al Islam Yogyakarta</option>
						<option value="Akademi Keperawatan Karya Husada Yogyakarta">Akademi Keperawatan Karya Husada Yogyakarta</option>
						<option value="Akademi Keperawatan Wiyata Husada Yogyakarta">Akademi Keperawatan Wiyata Husada Yogyakarta</option>
						<option value="Keperawatan YKY Yogyakarta">Akademi Keperawatan YKY Yogyakarta</option>
						<option value="Kesejahteraan Sosial AKK">Akademi Kesejahteraan Sosial AKK</option>
						<option value="Other">Other</option>
					<input id="Other" name="Other" type="text" placeholder="Other" value="<?php echo set_value('Univ');?>"> 
					</select>
					<?php echo form_error('Univ'); ?>
				</section>

				<section class="small-5 columns <?php echo form_error('Fakultas') ? 'error' : '' ;?>">
					FAKULTAS*<br>
					<input type="text" name="Fakultas" value="<?php echo set_value('Fakultas'); ?>">
					<?php echo form_error('Fakultas'); ?>
				</section>
			</div>

			<div class="row">
				<section class="small-5 columns">
					JURUSAN<br>
					<input type="text" name="Jurusan" value="<?php echo set_value('Jurusan'); ?>">
				</section>
				<section class="small-4 columns">
					PRODI<br>
					<input type="text" name="Prodi" value="<?php echo set_value('Prodi'); ?>">
				</section>
				<section class="small-3 columns <?php echo form_error('Angkatan') ? 'error' : '' ?>">
					ANGKATAN*<br>
					<input type="number" name="Angkatan" min="1" max="9999" value="<?php echo set_value('Angkatan'); ?>">
					<?php echo form_error('Angkatan'); ?>
				</section>
			</div>

			<div class="row">
				<section class="small-12 columns">
					Alamat Kampus<br>
					<input type="text" name="Alamat" value="<?php echo set_value('Alamat'); ?> "><br>
				</section>
			</div>
			<div class="row">
				<section class="small-12 columns">
				Prestasi Yang Pernah Dicapai<br>
					<input type="text" name="Prestasi" value="<?php echo set_value('Prestasi'); ?>">
				</section>
			</div>
		</div>

		<input type="hidden" name="time" value="<?php echo date("Y-d-m h:m:s");?>">
		<input type="hidden" name="tahap_2" value="sudah">
		<input type="hidden" name="status" value='3'>
		<div><input type="submit" value="Daftar" class="button radius right"></div>
</div>
</form>
