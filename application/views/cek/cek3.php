<div class="content" data-section-content>
	<form action="daftar/editlast" method="post" id= "form_id">
	<fieldset>
	<legend><h3>Data PPM</h3></legend>
			<div class="small-12 columns">
				<div class="cektext">
				<div class="row">
					<section class="small-4 columns>">
						Nomor Pendaftaran*<br>
						<input type="number" name="Daftar" min="1" max="9999" value="<?php echo $cek3->daftar; ?>">
					</section>
				</div>

				<div class="row">
					<section class="small-6 columns">
						Bulan Masuk PPM  <br>                
							<select name="Bulan">
								<option value="<?php echo $cek3->bulan?>"><?php echo $cek3->bulan; ?></option>
								<option value="Januari">Januari</option>
								<option value="Februari">Februari</option>
								<option value="Maret">Maret</option>
								<option value="April">April</option>
								<option value="Mei">Mei</option>
								<option value="Juni">Juni</option>
								<option value="Juli">Juli</option>
								<option value="Agustus">Agustus</option>
								<option value="September">September</option>
								<option value="Oktober">Oktober</option>
								<option value="November">November</option>
								<option value="Desember">Desember</option>
							</select><br>
					</section>
					<section class="small-6 columns <?php echo form_error('Tahun') ? 'error' : '' ; ?>">
						Tahun Masuk*<br>
						<input type="number" min="1" max="9999" name="Tahun" value="<?php echo $cek3->tahun; ?>">
					</section>
				</div>

				<div class="row">
					<section class="small-6 columns">
						Dapukan Sebelum Masuk PPM<br>
						<input type="text" name="Dapukan" value="<?php echo $cek3->dapukan; ?>">
					</section>
					<section class="small-6 columns">
						Status Jamaah*<br>
							<select name="Statusj" id="Statusj" >
								<option value="<?php echo $cek3->statusj; ?>"><?php echo $cek3->statusj;?></option>
								<option value="Lahir">Lahir</option>
								<option value="Tahun">Tahun</option>
							</select>
						<input type="text" name="Tahunj" id="Tahun" value="<?php echo $cek3->tahunj; ?>" placeholder="Tahun Jamaah" >
						
					</section>
			</div>

			<div class="row">

				<section class="small-6 columns">
						Status Mubaligh*<br>
							<select name="Statusm" id="Statusm" >
								<option value="<?php echo $cek3->statusm; ?>"><?php echo $cek3->statusm;?></option>
								<option value="Belum">Belum</option>
								<option value="Mubaligh">Mubaligh</option>
							</select>
						<input type="text" name="Tahunm" id="Mubaligh" value="<?php echo $cek3->tahunm; ?>" placeholder="Tahun Mubligh" >
						
					</section>

					<section class="small-6 columns">
						Status Tugas<br>
							<select name="Statust" id="Statust" >
								<option value="<?php echo $cek3->statust; ?>"><?php echo $cek3->statust;?></option>
								<option value="Belum">Belum Pernah Tugas</option>
								<option value="Mubaligh">Pernah Tugas</option>
							</select>
						<input type="text" name="Tugas" id="Pernah" value="<?php echo $cek3->tugas; ?>" placeholder="Tahun Mubligh" >
						
					</section>
			</div>

			<div class="row">
				<h5>Asal*</h5>
				<section class="small-4 columns">
					Kelompok*<br>
					<input type="text" name="Kelompok" value="<?php echo $cek3->kelompok; ?>">
					
				</section>

				<section class="small-4 columns">
					Desa*<br>
					<input type="text" name="Desa" value="<?php echo $cek3->desa ;?>">
					
				</section>
				<section class="small-4 columns"> 
					Daerah*<br>
					<input type="text" name="Daerah" value="<?php echo $cek3->daerah; ?>">
					<?php echo form_error('Daerah'); ?>
					<input type="hidden" name="time" value="<?php echo date("Y-d-m h:m:s");?>">
				</section>
			</div>
		</div>
		</div>
		</fieldset>
<br>
<div class="row">
				<section class="small-6 columns">
					<input type="hidden" name="time" value="<?php echo date("Y-d-m h:m:s");?>">
					
					<input type="hidden" name="daftar" value="<?php echo $daftar; ?>">

					<input type ="hidden" name="Part" value="Tahap_3">
					
					<input type="submit" value="Edit" class="small button alert left">
				</section>
				
				</form>
				<section class="small-6 columns">
					<form action="daftar/cek" method="post" >
					<input type="hidden" value="<?php echo $daftar; ?>" name="daftar">
					<input type="submit" value="Fixed" class="small button success right">
					</form>
				</section>
</div>
	</div>