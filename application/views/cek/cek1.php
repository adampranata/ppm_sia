
<div class="content" data-section-content>
		<form action="daftar/editlast" method="post">
    		<legend ><h3>Data Personal</h3></legend>
					<section class="cektext">
						<section class="row">
							<section class="large-3 columns <?php echo form_error('Daftar') ? 'error' : ''; ?>">
								Nomor Pendaftaran*
								<?php echo $cek1->daftar; ?>
								<input type="hidden" name="Daftar" id="Daftar" value="<?php echo $cek1->daftar; ?>">
							</section>
						</section>

						<div class="row">
							<fieldset>
								<legend>Biodata</legend>
									<div class="row">

										<section class="small-5 columns <?php echo form_error('Nama') ? 'error' : ''; ?>">
											Nama Lengkap *<br>               
											<input type="text" name="Nama" value="<?php echo $cek1->nama; ?>">
										</section>

										<section class="small-4 columns <?php echo form_error('Panggilan') ? 'error': ''; ?>">
											Nama Panggilan <br>
											<input type="text" name="Panggilan" value="<?php echo $cek1->panggilan; ?>">
										</section>
										<section class="small-3 columns <?php echo form_error('Kelamin') ? 'error' : ''; ?>">
							 				Jenis Kelamin *<br>
											<select name='Kelamin'>
												<option selected="selected" value="<?php echo $cek1->kelamin; ?>"><?php echo $cek1->kelamin; ?></option>
												<option value="L">L</option>
												<option value="P">P</option>
											</select>
										</section>
									</div>

									<div class="row">
										<section class="small-4 columns">
											Kota Lahir<br>
											<input type="text" name="Kotalahir" value="<?php echo $cek1->kotalahir; ?>"><br>
										</section>
										<section class="small-4 columns <?php echo form_error('Tanggal') ? 'error' : '' ?>">
											Tanggal Lahir* <br>
											<input type="date" name="Tanggallahir" value="<?php echo $cek1->tanggallahir; ?>">
										</section>
										<section class="small-4 columns <?php echo form_error('Darah') ? 'error' : '' ?>">
											Golongan Darah* <br>
											<select name="Darah">
												<option selected="selected" value="<?php echo $cek1->darah; ?>"><?php echo $cek1->darah; ?></option>
												<option value="Belum">Belum Tahu</option>
												<option value="A">A</option>
												<option value="B">B</option>
												<option value="AB">AB</option>
												<option value="O">O</option>
											</select>
										</section>
										<section class="small-12 columns">
											Hobby<br>
											<input type="text" name="Hobby" value="<?php echo $cek1->hoby; ?>">
										</section>
									</div>
								</fieldset>
							</div>

							<div class="row" id="printableArea">							
								<fieldset> 
									<legend>Alamat Asal*</legend>

										<section class="row">
												<section class="small-6 columns <?php echo form_error('Kabkot') ? 'error' : ''; ?>">
													Kabupaten / Kota*
													<select name="Kabkot" class="Custom">
														<option selected ="selected" value="<?php echo $cek1->kabkot; ?>"><?php echo $cek1->kabkot; ?></option>
														<option value="Kabupaten">Kabupaten</option>
														<option value="Kota">Kota</option>
													</select><br>
													<?php echo form_error('Kabkot'); ?>
												</section>

												<section class="small-6 columns <?php echo form_error('Namak') ? 'error' : ''; ?>">
													Nama Kabupaten / Kota*
													<input type="text" name="Namak" value="<?php echo $cek1->namak; ?>">	
													<?php echo form_error('Namak'); ?>
												</section>
										</section>

										<section class="row">

											<section class="small-6 columns <?php echo form_error('Kecamatan') ? 'error' : ''; ?>">
												Kecamatan* <br/>
												<input type = "text" name="Kecamatan" value="<?php echo $cek1->kecamatan; ?>">
												<?php echo form_error('Kecamatan'); ?>
											</section>

											<section class="small-6 columns <?php echo form_error('Kelurahan') ? 'error' : ''; ?>">
												Kelurahan / Desa *
												<input type="text" name="Kelurahan" value="<?php echo $cek1->kelurahan; ?>">
												<?php echo form_error('Kelurahan'); ?>
											</section>
										</section>


										<section class="row">
											<section class="small-9 columns <?php echo form_error('Alengkap') ? 'error' : '' ; ?>">
												Alamat Lengkap*<br>
												<input type="text" placeholder="contoh : Jl. D.I. Panjaitan no. 294, RT. 05 Rw. 03" name="Alengkap" value="<?php echo $cek1->alengkap; ?>">
												<?php echo form_error('Alengkap');?>
											</section>

											<section class="small-3 columns">
												Kode Pos<br>
												<input type="text" name="Pos" value="<?php echo $cek1->pos; ?>">
											</section>
										</section>
								</fieldset>
								<input type="button" onclick="printDiv('printableArea')" value="print this section">
							</div><br />

							<div class="row">
								<fieldset>
									<legend>Kontak</legend>
										<div class ="row">

											<section class="large-6 columns <?php echo form_error('Telp') ? 'error' : '' ?>">
												Nomor Telepon/HP *
												<input type="tel" name="Telp" value="<?php echo $cek1->telp; ?>">
											</section>

											<section class="large-6 columns">
												E-mail<br>
												<input type="text" name="Email" value="<?php echo $cek1->email; ?>">
											</section>
										</div>

										<div class="row">
											<section class="small-6 columns">
												Facebook<br>
												<input type="text" name="Facebook" value="<?php echo $cek1->facebook; ?>">
											</section>

											<section class="small-6 columns">
												Twitter<br>
												<input type="text" name="Twitter" value="<?php echo $cek1->twitter; ?>">
											</section>
										</div>

								</fieldset> <br / >
							</div>

			<section class="row">
				<section class="small-6 columns">
					<input type="hidden" name="time" value="<?php echo date("Y-d-m h:m:s");?>">
					<input type="hidden" name="tahap_1" value="sudah">
					<input type="hidden" name="daftar" value="<?php echo $daftar; ?>">
					<input type="hidden" name="Part" value='Tahap_1'>
					<input type="submit" value="Edit" class="button alert left">
				
				</form>
				</section>
				<section class="small-6 columns">
					<form action="daftar/cek" method="post" >
					<input type="hidden" value="<?php echo $daftar; ?>" name="daftar">
					<input type="submit" value="Fixed" class="button success right">
					</form>
				</section>
			</section><br>
</div>

<script type="text/javascript">
	function printDiv(divName)
	{
		varprintContents = document.getElementById(divName).innerHTML;

		varoriginalContents = 	document.body.innerHTML;

		document.body.innerHTML = printContents;

		window.print();

		document.body.innerHTML = originalContents;
	}
</script>

