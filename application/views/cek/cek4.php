<div class="content" data-section-content>
	<form action="daftar/editlast" method="post" id= "form_id">		
    <legend><h3 class="form-signin-heading">Data Perwalian Santri</h3></legend>

    <div class="cektext">

				<div class="row">
					<section class="small-4 columns">
						Nomor Pendaftaran* <br>
						<input type="number" name="Daftar" min="1" max="9999" value="<?php echo $cek4->daftar; ?>">
					
					</section>
				</div>

				<div class="row">
					<fieldset>
						<legend>Biodata Orang Tua/ Wali*</legend>

				<div class="row">
					<section class="small-12 columns">
					<section class="small-6 columns">
						Identitas Orang tua / Wali * <br>
							<select name="Jenis">
								<option value="<?php echo $cek4->jenis; ?>"><?php echo $cek4->jenis; ?></option> 
								<option value="Ayah">Ayah</option>
								<option value="Ibu">Ibu</option>
								<option value="Wali">Wali</option>
							</select>
							
					</section>

					<section class="small-6 columns">
						Nama Orang Tua / Wali * <br/>
						<input type="text" name="Nama" value="<?php echo $cek4->nama; ?>">
						
					</section>
				</section>
			</div>

				<div class="row">
					<section class="small-6 columns">
						Dapukan Orang Tua / Wali <br>
						<input type="text" name="Dapukan" value="<?php echo $cek4->dapukan; ?>"><br>
						Email Orang Tua / Wali <br/>
						<input type="text" name="Email" value="<?php echo $cek4->email; ?>">
					</section>

					<section class="small-6 columns">
						Professi Orang Tua / Wali <br>
						<input type="text" name="Professi" value="<?php echo $cek4->professi; ?>"><br>
					</section>
				</div>

			<div class="row">
					<section class="small-12 columns">
					<section class="small-6 columns">
						Status Orang Tua / Wali * <br />
						<select name="Status">
								<option value="<?php echo $cek4->status; ?>"><?php echo $cek4->status; ?></option> 
								<option value="jamaah">Jama'ah</option>
								<option value="belum">Belum</option>
						</select>
					</section>
					<section class="small-6 columns">
						Nomor Telpon Orang Tua / Wali * <br/>
							<input type="number" name="Telp" value="<?php echo $cek4->telp; ?>">
							
					</section>
				</section>
				</div>
			</fieldset>
		</div>


				<div class="row">							
					<fieldset> 
						<legend>Alamat Asal Orang Tua/Wali*</legend>

						<section class="row">
							<section class="small-6 columns">
								Kabupaten / Kota*
								<select name="Kabkoto" class="Custom">
									<option selected ="selected" value="<?php echo $cek4->kabkoto; ?>"><?php echo $cek4->kabkoto ?></option>
									<option value="Kabupaten">Kabupaten</option>
									<option value="Kota">Kota</option>
								</select><br>
							</section>

							<section class="small-6 columns">
								Nama Kabupaten / Kota*
								<input type="text" name="Namako" value="<?php echo $cek4->namako; ?>">	
							</section>
						</section>

						<section class="row">

							<section class="small-6 columns">
								Kecamatan* <br/>
								<input type = "text" name="Kecamatano" value="<?php echo $cek4->kecamatano; ?>">
							</section>

							<section class="small-6 columns">
								Kelurahan / Desa *
							<input type="text" name="Kelurahano" value="<?php echo $cek4->kelurahano; ?>">
							
						</section>
					</section>

					<section class="row">
						<section class="small-9 columns">
							Alamat Lengkap*<br>
							<input type="text" placeholder="contoh : Jl. D.I. Panjaitan no. 294, RT. 05 Rw. 03" name="Alengkapo" value="<?php echo $cek4->alengkapo; ?>">
						</section>

						<section class="small-3 columns">
							Kode Pos<br>
							<input type="text" name="Pos" value="<?php echo $cek4->pos; ?>">
						</section>
						</fieldset>
					</div><br />

				<div class="row">							
					<fieldset> 
						<legend>Data Pengurus Asal*</legend>

						<section class="row">
							

							<section class="small-4 columns">
								Nama Pengurus Asal 
								<input type="text" name="Namap" value="<?php echo $cek4->namap; ?>">	
							</section>
						
							<section class="small-4 columns">
								Dapukan <br/>
								<input type = "text" name="Dapukanp" value="<?php echo $cek4->dapukanp; ?>">
							</section>

							<section class="small-4 columns">
								Nomor telepon
							<input type="text" name="Telpp" value="<?php echo $cek4->telpp; ?>">
							<?php echo form_error('Asal');?>
						</section>

						</section>
						</fieldset> <br />
					</div>
				</div>
				<div class="row">

				<section class="small-6 columns">
				<input type="hidden" name="time" value="<?php echo date("Y-d-m h:m:s");?>">
					
					<input type="hidden" name="daftar" value="<?php echo $daftar; ?>">

					<input type="hidden" name="Part" value="Tahap_4">
					
					<input type="submit" value="Edit" class="small button alert left">
				</section>

				</form>
					<section class="small-6 columns">
					<form action="daftar/cek" method="post" >
					<input type="hidden" value="<?php echo $daftar; ?>" name="daftar">
					<input type="submit" value="Fixed" class="small button success right"><br>
					</form>
					</section>
				</div>


	</div>
