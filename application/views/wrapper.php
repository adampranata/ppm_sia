<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8" />
  <base href="<?php echo base_url(); ?>">
  <!-- Set the viewport width to device width for mobile -->
  <meta name="viewport" content="width=device-width" />

  <title><?php echo $title; ?> - PPM </title>
  <!-- Included CSS Files (Uncompressed)
  <link rel="stylesheet" href="stylesheets/foundation.css">
  -->
  
  <!-- Included CSS Files (Compressed) -->
  <link rel="stylesheet" href="css/foundation.min.css" >
  <link rel="stylesheet" href="css/foundation.css" >
  <link rel="stylesheet" href="css/normalize.css" >
  <link rel="shortcut icon" href="img/favicon.ico"/>
  <link rel="stylesheet" type="text/css" href="css/demo.css" />
  <link rel="stylesheet" type="text/css" href="css/style1.css" />
  

  <!--<link rel="stylesheet" href="stylesheets/app.css">-->
  <script src="js/custom.modernizr.js"></script>
    <link rel="shortcut icon" href="../favicon.ico">

</head>
<body>  
  
    
<div class="small-12 columns">

  <div class="row">
 
    <ul id="featured2" data-orbit>
      <li data-orbit-slide="headline-1">
        <img src="">
        <h2>Headline 1</h2>
        <h3>Subheadline</h3>
        <p>Pellentesque habitant morbi tristique senectus.</p>
        <a href="#" class="button radius right">COBA</a>
      </li>
      <li data-orbit-slide="headline-2">
        <h2>Headline 2</h2>
        <h3>Subheadline</h3>
        <p>Pellentesque habitant morbi tristique senectus.</p>
      </li>
      <li data-orbit-slide="headline-3">
        <h2>Headline 3</h2>
        <h3>Subheadline</h3>
        <p>Pellentesque habitant morbi tristique senectus.</p>
      </li>
      <li data-orbit-slide="headline-4">
        <h2>Headline 4</h2>
        <h3>Subheadline</h3>
        <p>Pellentesque habitant morbi tristique senectus.</p>
      </li>
      <li data-orbit-slide="headline-5">
        <h2>Headline 5</h2>
        <h3>Subheadline</h3>
        <p>Pellentesque habitant morbi tristique senectus.</p>
      </li>
    </ul>
  </div>

      <div class="row">
        <div class="small-12 columns">
          <div class="small-3 columns">
        <?php echo $this->load->view('navigation'); ?>
      </div>
      <div class="small-9 columns">
        <?php echo $content; ?> 
      </div>
      </div>
    </div>
  </div>
  <div class="large-12 columns">
    <?php echo $this->load->view('footer'); ?>
  </div>
    <!--<script src="js/jquery-1.8.2.min.js"></script>
    <script src="js/bootstrap.min.js"></script>-->

    <script src="js/foundation/foundation.js"></script>
  
  <script src="js/foundation/foundation.dropdown.js"></script>
  
  <script src="js/foundation/foundation.placeholder.js"></script>
  
  <script src="js/foundation/foundation.forms.js"></script>
  
  <script src="js/foundation/foundation.alerts.js"></script>
  
  <script src="js/foundation/foundation.magellan.js"></script>
  
  <script src="js/foundation/foundation.reveal.js"></script>
  
  <script src="js/foundation/foundation.tooltips.js"></script>
  
  <script src="js/foundation/foundation.clearing.js"></script>
  
  <script src="js/foundation/foundation.cookie.js"></script>
  
  <script src="js/foundation/foundation.joyride.js"></script>
  
  <script src="js/foundation/foundation.orbit.js"></script>
  
  <script src="js/foundation/foundation.section.js"></script>
  
  <script src="js/foundation/foundation.topbar.js"></script>

  

<script src="js/foundation/foundation.orbit.js"></script>
<script src="js/foundation/foundation.alerts.js"></script>
<script src="js/foundation/foundation.js"></script>
  
  <!-- Included JS Files (Compressed) -->
  
  <script src="js/vendor/jquery.js"></script>
  <script src="js/foundation.min.js"></script>
  <script src="js/vendor/zepto.js"></script>
  
  <!-- Initialize JS Plugins -->
  <script src="javascripts/app.js"></script>

  <script>
    $(document).foundation();
  </script>

 <!-- <script type="text/javascript">
     $(window).load(function() {
         $('#slider').orbit();
     });
  </script>-->

  <script>
  document.write('<script src=/js/vendor/'
  +('__proto__' in {} ? 'zepto' : 'jquery')
  + '.js><\/script>');
  </script>
</body>
</html>