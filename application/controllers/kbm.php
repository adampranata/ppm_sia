<?php

class kbm extends CI_Controller
{
	function __construct()
	{
		parent:: __construct();
	}

	function index() {
		 //if($this->session->userdata('logged_in'))
	//{

		$this->db->select('*');
		$this->db->where('tahap_1','sudah');
		$query = $this->db->get('pendaftaran');
		$list = array();

		if ($query->num_rows() > 0){
			foreach ($query->result_array() as $rows) {
				$list[]= $rows;
			}
		}

		$query->free_result();


		$admin['pendaftar'] = $list;

		$this->db->select('*');
		$this->db->where('pesan !=','');
		$sql = $this->db->get('pendaftaran');
		$daftar = array();

		if ($sql->num_rows() > 0){
			foreach ($sql->result_array() as $rows) {
				$daftar[] = $rows;
			}
		}

		$sql->free_result();

		$admin['message'] = $daftar;

		$data = array(
			'title' => 'Daftar Penghuni PPM', 
			$admin);

		$data['content'] = $this->load->view('kbm/kbm-page',$admin , true);
		$this->load->view('wrapper3', $data);

		//}
		//else redirect('/login');
	}

	function logout()
 {
   $this->session->unset_userdata('logged_in');
   session_destroy();
   redirect('kbm', 'refresh');
 }

}
