<?php

class daftar extends CI_Controller 
{
	function __construct()
	{
		parent:: __construct();
        $this->load->library('Form_validation');
        $this->load->library('session');
        $this->load->model('profile_m');
	}

	function index(){
			 if (! $_POST) {
            $data = array(
                'title' => 'Pendaftaran Mahasiswa Baru',
                'content' => $this->load->view('form/formulir.php', null, true)
            );
            $this->load->view('wrapper2', $data);
        } else {
        	$this->daftarin();
        }
    }


    function daftarin(){

        $this->form_validation->set_rules('Daftar', 'Nomor Pendaftaran', 'required');

        $this->form_validation->set_rules('Nama','Nama Pendaftar','required');

        $this->form_validation->set_rules('Panggilan', 'Nama Panggilan', 'required');

        $this->form_validation->set_rules('Kelamin', 'Jenis Kelamin', 'required');

        $this->form_validation->set_rules('Tanggallahir', 'Tanggal Lahir', 'required');
        
        $this->form_validation->set_rules('Darah', 'Golongan Darah', 'required');
        
        $this->form_validation->set_rules('Telp', 'Nomor Telepon', 'required');

        $this->form_validation->set_rules('Alengkap', 'Alamat Lengkap', 'required');

        $this->form_validation->set_rules('Kelurahan', 'Kelurahan Asal', 'required');

        $this->form_validation->set_rules('Kabkot', 'Kabupaten / Kota', 'required');

        $this->form_validation->set_rules('Kecamatan', 'Nama Kecamatan Asal', 'required');

        $this->form_validation->set_rules('Namak', 'Nama Kabupaten / Kota', 'required');

        $this->form_validation->set_error_delimiters('<small>','</small>');

        if($this->form_validation->run() == FALSE)
        {
           $data = array(
                'title' => 'Pendaftaran Mahasiswa Baru',
                'content' => $this->load->view('form/formulir.php', null, true)
            );
            $this->load->view('wrapper2', $data);
        }


        else{

        $daftar = $this->input->post('Daftar');
        $query = $this->db->get_where('personal', array('daftar' => $daftar));

            //ini memastikan data yang dikirimkan tidak merusak data yang telah ada , juga menjadi handling dari error massage mysql
             if ($query->num_rows() > 0){
                $pesan = 'Maaf nomor pendaftaran yang anda masukan salah atau sudah terdaftar silahkan cobalagi atau kirim pesan kepada panitia' ;
                $error = 'error';

                $bricks = array(
                    'pesan' => $pesan,
                    'error' => $error,
                    'title' => 'Pendaftaran Mahasiswa Baru');

                $data['content'] = $this->load->view('form/formulir.php', $bricks, true);
                $this->load->view('wrapper2', $data);
                }

            //saat data dipastikan tidak bentrok dengan data yang lain maka lanjut,     
            else {
                $pesan = '';
                $error = '';


            	$data = array(
                    'daftar' => $_POST['Daftar'],
                    'nama' => $_POST['Nama'],
                    'panggilan' => $_POST['Panggilan'],
                    'kelamin' => $_POST['Kelamin'],
                    'kotalahir' => $_POST['Kotalahir'],
                    'tanggallahir' => $_POST['Tanggallahir'],
                    'darah' => $_POST['Darah'],
                    'telp' => $_POST['Telp'],
                    'pos' => $_POST['Pos'],
                    'email' => $_POST['Email'],
                    'facebook' => $_POST['Facebook'],
                    'twitter' => $_POST['Twitter'],
                    'hoby' => $_POST['Hobby'],
                    'kabkot' => $_POST['Kabkot'],
                    'kecamatan' =>  $_POST['Kecamatan'],
                    'kelurahan' => $_POST['Kelurahan'],
                    'namak' => $_POST['Namak'],
                    'alengkap' => $_POST['Alengkap'],


                    );

                $this->db->insert('personal', $data);

        //bagian ini menentukan posisi pendaftar sampai dengan tahap mana atau mungkin ada tahap yang terlewat
        $data = array(
        'tahap_1' => $_POST['tahap_1'],
        'status' => $_POST['status']);
        $this->db->where('id_daftar',$daftar);
        $this->db->update('pendaftaran',$data);

            $data['title'] = 'sukses';
            $data['content'] = $this->load->view('form/sukses', $bricks , true);
            $this->load->view('wrapper2', $data);
        }
    }
}

    function edit(){

        $daftar = $this->input->post('Daftar');
        $tahap1 = $this->db->select('*')->where('daftar',$daftar)->from('personal')->get()->row();

        $data = array(
            'title' => 'Edit',
            'tahap1' => $tahap1);
        $data['content'] = $this->load->view('form/edit', $data, true);
        $this->load->view('wrapper2', $data);

    }


    function edita(){

        $this->form_validation->set_rules('Daftar', 'Nomor Pendaftaran', 'required');

        $this->form_validation->set_rules('Nama','Nama Pendaftar','required');

        $this->form_validation->set_rules('Panggilan', 'Nama Panggilan', 'required');

        $this->form_validation->set_rules('Kelamin', 'Jenis Kelamin', 'required');

        $this->form_validation->set_rules('Asal', 'Kota Asal', 'required');

        $this->form_validation->set_rules('Tanggal', 'Tanggal Lahir', 'required');
        
        $this->form_validation->set_rules('Darah', 'Golongan Darah', 'required');
        
        $this->form_validation->set_rules('Telp', 'Nomor Telepon', 'required');

        $this->form_validation->set_error_delimiters('<small>','</small>');

        $daftar = $this->input->post('Daftar');
        $tahap1 = $this->db->select('*')->where('daftar',$daftar)->from('personal')->get()->row();

        if($this->form_validation->run() == FALSE){
        $data = array(
            'title' => 'Edit',
            'tahap1' => $tahap1);
        $data['content'] = $this->load->view('form/edit', $data, true);
        $this->load->view('wrapper2', $data);
    }

    else {
        $daftar = $this->input->post('Daftar');
        $query = $this->db->get_where('personal', array('daftar' => $daftar));

        if ($query->num_rows() < 1){
            $pesan = 'Maaf nomor pendaftaran yang anda masukan salah atau belum terdaftar' ;
            $error = 'error';

            $bricks = array(
                'pesan' => $pesan,
                'error' => $error,
                'title' => 'Pendaftaran Mahasiswa Baru');

            $data['content'] = $this->load->view('form/formulir.php', $bricks, true);
            $this->load->view('wrapper2', $data);
        }


        else {
            $pesan = '' ;
            $error = '';


                $bricks = array(
                    'daftar' => $this->input->post('Daftar'),
                    'nama' => $this->input->post('Nama'),
                    'panggilan' => $this->input->post('Panggilan'),
                    'kelamin' => $this->input->post('Kelamin'),
                    'kotalahir' =>$this->input->post('Kota'),
                    'tanggallahir' => $this->input->post('Tanggal'),
                    'darah' => $this->input->post('Darah'),
                    'telp' => $this->input->post('Telp'),
                    'asal' => $this->input->post('Asal'),
                    'pos' => $this->input->post('Pos'),
                    'email' => $this->input->post('Email'),
                    'facebook' => $this->input->post('Facebook'),
                    'twitter' => $this->input->post('Twitter'),
                    'hoby' => $this->input->post('Hobby')
                );

        $this->db->where('daftar',$daftar);        
        $this->db->update('personal', $bricks);

        $this->db->where('id_daftar',$daftar);
        $data = array(
        'status' => $_POST['status']);
        $this->db->update('pendaftaran',$data);

            $data['title'] = 'sukses';
            $data['content'] = $this->load->view('form/sukses', $bricks , true);
            $this->load->view('wrapper2', $data);
            }
        }


    }

    function daftarin2(){
        if (! $_POST){
            $data = array(
                'title' => 'Pendaftara Mahasiswa Baru',
                'content' => $this->load->view('form/formulir2.php', null, true)
            );
            $this->load->view('wrapper2', $data);
             } else {
            $this->daftarin2a();
        }
}

         function daftarin2a(){

            $this->form_validation->set_rules('Daftar', 'Nomor Pendaftaran', 'required');
            $this->form_validation->set_rules('Univ', 'Nama Universitas', 'required');
            $this->form_validation->set_rules('Fakultas', 'Nama Fakultas', 'required');
            $this->form_validation->set_rules('Angkatan', 'Tahun Angkatan', 'required');
            $this->form_validation->set_error_delimiters('<small>', '</small>');

            if($this->form_validation->run() == FALSE)
        {
           $data = array(
                'title' => 'Pendaftaran Mahasiswa Baru',
                'content' => $this->load->view('form/formulir2.php', null, true)
            );
            $this->load->view('wrapper2', $data);
        }

        else {

            $daftar = $this->input->post('Daftar');
            $query = $this->db->get_where('kuliah', array('daftar' => $daftar));

        if ($query->num_rows() > 0){
            $pesan = 'Maaf nomor pendaftaran yang anda masukan salah atau sudah terdaftar silahkan cobalagi atau kirim pesan kepada panitia' ;
            $error = 'error';

            $bricks = array(
                'pesan' => $pesan,
                'error' => $error,
                'title' => 'Pendaftaran Mahasiswa Baru');

            $data['content'] = $this->load->view('form/formulir2.php', $bricks, true);
            $this->load->view('wrapper2', $data);
        }


        else {
            $pesan = '' ;
            $error = '';


        $data = array(
            'daftar' => $_POST['Daftar'],
            'univ' => $_POST['Univ'],
            'fak' => $_POST['Fakultas'],
            'jur' => $_POST['Jurusan'],
            'prodi' => $_POST['Prodi'],
            'angkatan' => $_POST['Angkatan'],
            'alamat' => $_POST['Alamat'],
            'prestasi' => $_POST['Prestasi']);
        $this->db->insert('kuliah', $data);

        $daftar = $this->input->post('Daftar');
        $data = array(
        'tahap_2' => $_POST['tahap_2'],
        'status' => $_POST['status']);
        $this->db->where('id_daftar',$daftar);
        $this->db->update('pendaftaran',$data);
            $bricks = array(
                'pesan' => $pesan,
                'error' => $error,
                'title' => 'Pendaftaran Mahasiswa Baru');

            $data['content'] = $this->load->view('form/sukses2.php', $bricks, true);
            $this->load->view('wrapper2', $data);
        }
    }
}


function daftarin3(){
        if (! $_POST){
            $data = array(
                'title' => 'Pendaftaran Mahasiswa Baru',
                'content' => $this->load->view('form/formulir3.php', null, true)
            );
            $this->load->view('wrapper2', $data);
             } else {
            $this->daftarin3a();
        }
}

         function daftarin3a(){

            $this->form_validation->set_rules('Daftar', 'Nomor Pendaftaran', 'required');
            $this->form_validation->set_rules('Tahun', 'Tahun Masuk', 'required');
            $this->form_validation->set_rules('Kelompok', 'Asal Kelompok', 'required');
            $this->form_validation->set_rules('Desa', 'Asal Desa', 'required');
            $this->form_validation->set_rules('Daerah', 'Asal Daerah', 'required');
            $this->form_validation->set_rules('Statusj', 'Status Jamaah', 'required');
            $this->form_validation->set_rules('Statusm', 'Status Mubaligh', 'required');
            $this->form_validation->set_error_delimiters('<small>','</small>');

    if($this->form_validation->run() == FALSE)
            {
                $data= array(
                    'title' => 'Pendaftaran Mahasiswa Baru',
                    'content' => $this->load->view('form/formulir3.php', null, true)
                    );
                $this->load->view('wrapper2', $data);
            }

    else {

            $daftar = $this->input->post('Daftar');
            $query = $this->db->get_where('ppm', array('daftar' => $daftar));

        if ($query->num_rows() > 0){
            $pesan = 'Maaf nomor pendaftaran yang anda masukan salah atau sudah terdaftar silahkan cobalagi atau kirim pesan kepada panitia' ;
            $error = 'error';

            $bricks = array(
                'pesan' => $pesan,
                'error' => $error,
                'title' => 'Pendaftaran Mahasiswa Baru');

            $data['content'] = $this->load->view('form/formulir3.php', $bricks, true);
            $this->load->view('wrapper2', $data);
        }


        else {
            $pesan = '' ;
            $error = '';

        $data = array(
            'daftar' => $_POST['Daftar'],
            'bulan' => $_POST['Bulan'],
            'tahun' => $_POST['Tahun'],
            'dapukan' => $_POST['Dapukan'],
            'statusj' => $_POST['Statusj'],
            'tahunj' => $_POST['Tahunj'],
            'statusm' => $_POST['Statusm'],
            'tahunm' => $_POST['Tahunm'],
            'kelompok' => $_POST ['Kelompok'],
            'desa' => $_POST ['Desa'],
            'daerah' => $_POST ['Daerah'],
            'statust' => $_POST['Statust'],
            'tugas' => $_POST['Tugas'],
            );

        $this->db->insert('ppm', $data);

        $daftar = $this->input->post('Daftar');
        $data = array(
        'tahap_3' => $_POST['tahap_3'],
        'status' => $_POST['status']);
        $this->db->where('id_daftar',$daftar);
        $this->db->update('pendaftaran',$data);

            $bricks =array(
                'pesan' => $pesan,
                'error' => $error);

            $data = array(
                'title' => 'Sukses',
                'content' => $this->load->view('form/sukses3',$bricks,true)
                );
            $this->load->view('wrapper2',$data);
        }
    }
}

     function daftarin4(){
        if (! $_POST){
            $data = array(
                'title' => 'Pendaftara Mahasiswa Baru',
                'content' => $this->load->view('form/formulir4.php', null, true)
            );
            $this->load->view('wrapper2', $data);
             } else {
            $this->daftarin4a();
        }
}

         function daftarin4a(){

        $this->form_validation->set_rules('Daftar', 'Nomor Pendaftaran', 'required');

        $this->form_validation->set_rules('Jenis', 'Identitas Orang Tua / Wali', 'required');

        $this->form_validation->set_rules('Nama', 'Nama Orang Tua / Wali', 'required');

        $this->form_validation->set_rules('Status', 'Status Orang Tua/ Wali', 'required');

        $this->form_validation->set_rules('Telp', 'Nomor Orang Tua / Wali', 'required');

        $this->form_validation->set_rules('Alengkapo', 'Alamat Orang Tua', 'required');

        $this->form_validation->set_rules('Kecamatano', 'Kecamatan', 'required');

        $this->form_validation->set_rules('Kelurahano', 'Kelurahan', 'required');

        $this->form_validation->set_rules('Namap', 'Nama Pengurus Asal', 'required');

        $this->form_validation->set_rules('Telpp', 'Nomor Telpon Pengurus Asal', 'required');

        $this->form_validation->set_error_delimiters('<small>', '</small>');
        if($this->form_validation->run() == FALSE)
            {
                $data= array(
                    'title' => 'Pendaftaran Mahasiswa Baru',
                    'content' => $this->load->view('form/formulir4.php', null, true)
                    );
                $this->load->view('wrapper2', $data);
            }

    else {

            $daftar = $this->input->post('Daftar');
            $query = $this->db->get_where('wali', array('daftar' => $daftar));

        if ($query->num_rows() > 0){
            $pesan = 'Maaf nomor pendaftaran yang anda masukan salah atau sudah terdaftar silahkan cobalagi atau kirim pesan kepada panitia' ;
            $error = 'error';

            $bricks = array(
                'pesan' => $pesan,
                'error' => $error,
                'title' => 'Pendaftaran Mahasiswa Baru');

            $data['content'] = $this->load->view('form/formulir4.php', $bricks, true);
            $this->load->view('wrapper2', $data);
        }


        else {
            $pesan = '' ;
            $error = '';        



        $data = array(
            'daftar' => $_POST['Daftar'],
            'jenis' => $_POST['Jenis'],
            'nama' => $_POST['Nama'],
            'status' => $_POST['Status'],
            'pos' => $_POST['Pos'],
            'telp' => $_POST['Telp'],
            'email' => $_POST['Email'],
            'professi' => $_POST ['Professi'],
            'dapukan' => $_POST ['Dapukan'],
            'kabkoto' => $_POST ['Kabkoto'],
            'kelurahano' => $_POST ['Kelurahano'],
            'kecamatano' => $_POST ['Kecamatano'],
            'alengkapo' => $_POST ['Alengkapo'],
            'namako' => $_POST ['Namako'],
            'namap' => $_POST ['Namap'],
            'dapukanp' => $_POST ['Dapukanp'],
            'telpp' => $_POST ['Telpp'] );
        $this->db->insert('wali', $data);

        $daftar = $this->input->post('Daftar');
        $data = array(
        'tahap_4' => $_POST['tahap_4'],
        'status' => $_POST['status']);
        $this->db->where('id_daftar',$daftar);
        $this->db->update('pendaftaran',$data);

        $bricks = array(
            'title' => 'Sukses',
            'daftar' => $daftar);

            $data['content'] = $this->load->view('form/sukses4',$bricks,true);
            $this->load->view('wrapper2',$data);
        }
    }
}

function cek(){

    $this->load->model('profile_m');
    $daftar = $_POST['daftar'];

    if ($daftar == "")
    {
        $daftar == "0" ;
    }


    $cek1 = $this->db->get_where('personal', array('daftar' => $daftar));
    {
        if ($cek1->num_rows() < 1){
            $tahap1 = 'kosong1';
        }

        elseif ($cek1->num_rows() == 1) {
            $tahap1 = 'cek1';
        }
    }

   $cek2 = $this->db->get_where('kuliah', array('daftar' => $daftar));
   {

        if($cek2->num_rows() < 1 ){
            $tahap2 = 'kosong2';
        }

        elseif ($cek2->num_rows() == 1){
            $tahap2 = 'cek2';
        }
   }

   $cek3 = $this->db->get_where('ppm', array('daftar' => $daftar));
   {

        if($cek3->num_rows() < 1) {
            $tahap3 = 'kosong3';
        }
        elseif($cek3->num_rows() == 1){
            $tahap3 = 'cek3';
        }
    }

    $cek4 = $this->db->get_where('wali', array('daftar' => $daftar));
    {
        if($cek4->num_rows() < 1) {
            $tahap4 = 'kosong4';
        }

        elseif($cek4->num_rows() == 1) {
            $tahap4 = 'cek4';
        }
    }

   $bricks = array(
                    'title' => 'cek',
                    'daftar' => $daftar,
                    'cek1' => $cek1,
                    'cek2' => $cek2,
                    'cek3' => $cek3,
                    'cek4' => $cek4,
                    'tahap1' => $tahap1,
                    'tahap2' => $tahap2,
                    'tahap3' => $tahap3,
                    'tahap4' => $tahap4,
                    'daftar'  => $daftar);

   $data['content'] = $this->load->view('cek/cek', $bricks,true);
   $this->load->view('wrapper-cek.php',$data);
}

function cek_by_name(){

    $this->load->model('profile_m');
    $nama = $_POST['Nama'];

    $cek1 = $this->db->select('*')->from('personal')->where("nama LIKE '%$nama%'")->get();
    {
        if ($cek1->num_rows() < 1){
            $tahap1 = 'kosong1';
        }

        elseif ($cek1->num_rows() >= 1) {
            $tahap1 = 'cekn';
        }
    }

  $bricks = array(
                    'title' => 'cek',
                    'nama' => $nama,
                    'cek1' => $cek1,
                    'tahap1' => $tahap1,
                    );

   $data['content'] = $this->load->view('cek/cek_nama', $bricks,true);
   $this->load->view('wrapper-cek.php',$data);
}

    function pesan(){
        $id = $this->input->post('id_daftar');
        $data = array(
            'pesan' => $_POST['pesan']);

        $this->db->where('id_daftar',$id);
        $this->db->update('pendaftaran',$data);
        $link = $this->db->select('status')->from('pendaftaran')->where('id_daftar',$id)->get()->row();
        if ($link->status == 1 ) {
            redirect('daftar');
        }
        elseif ($link->status == 2) {
            redirect('daftar/daftarin2');
        }elseif ($link->status == 3) {
            redirect('daftar/daftarin3');
        }elseif($link->status == 4){
        redirect("daftar/daftarin4");
    }
        else{
            redirect("#");
        }
    }

    function editlast (){

        $daftar = $_POST['daftar'];
        $part = $_POST['Part'];

        if ($part == 'Tahap_1') {
        

        $data = array(
                    
                    'nama' => $_POST['Nama'],
                    'panggilan' => $_POST['Panggilan'],
                    'kelamin' => $_POST['Kelamin'],
                    'kotalahir' => $_POST['Kotalahir'],
                    'tanggallahir' => $_POST['Tanggallahir'],
                    'darah' => $_POST['Darah'],
                    'telp' => $_POST['Telp'],
                    'pos' => $_POST['Pos'],
                    'email' => $_POST['Email'],
                    'facebook' => $_POST['Facebook'],
                    'twitter' => $_POST['Twitter'],
                    'hoby' => $_POST['Hobby'],
                    'kabkot' => $_POST['Kabkot'],
                    'kecamatan' =>  $_POST['Kecamatan'],
                    'kelurahan' => $_POST['Kelurahan'],
                    'namak' => $_POST['Namak'],
                    'alengkap' => $_POST['Alengkap'],);
        $this->db->where('daftar',$daftar);
        $this->db->update('personal', $data); 
        
        }

        elseif ($part == 'Tahap_2') {
            

        $data = array(
            'univ' => $_POST['Univ'],
            'fak' => $_POST['Fakultas'],
            'jur' => $_POST['Jurusan'],
            'prodi' => $_POST['Prodi'],
            'angkatan' => $_POST['Angkatan'],
            'alamat' => $_POST['Alamat'],
            'prestasi' => $_POST['Prestasi']);
        $this->db->where('daftar',$daftar);
        $this->db->update('kuliah', $data);
        
        }

        elseif ($part == 'Tahap_3') {
            

         $data = array(
            'daftar' => $_POST['Daftar'],
            'bulan' => $_POST['Bulan'],
            'tahun' => $_POST['Tahun'],
            'dapukan' => $_POST['Dapukan'],
            'statusj' => $_POST['Statusj'],
            'tahunj' => $_POST['Tahunj'],
            'statusm' => $_POST['Statusm'],
            'tahunm' => $_POST['Tahunm'],
            'kelompok' => $_POST ['Kelompok'],
            'desa' => $_POST ['Desa'],
            'daerah' => $_POST ['Daerah'],
            'statust' => $_POST['Statust'],
            'tugas' => $_POST['Tugas'],
            );
         $this->db->where('daftar',$daftar);
        $this->db->update('ppm', $data);
        }

        elseif ($part == 'Tahap_4') {

        $data = array(
            'daftar' => $_POST['Daftar'],
            'jenis' => $_POST['Jenis'],
            'nama' => $_POST['Nama'],
            'status' => $_POST['Status'],
            'pos' => $_POST['Pos'],
            'telp' => $_POST['Telp'],
            'email' => $_POST['Email'],
            'professi' => $_POST ['Professi'],
            'dapukan' => $_POST ['Dapukan'],
            'kabkoto' => $_POST ['Kabkoto'],
            'kelurahano' => $_POST ['Kelurahano'],
            'kecamatano' => $_POST ['Kecamatano'],
            'alengkapo' => $_POST ['Alengkapo'],
            'namako' => $_POST ['Namako'],
            'namap' => $_POST ['Namap'],
            'dapukanp' => $_POST ['Dapukanp'],
            'telpp' => $_POST ['Telpp'] );
        $this->db->where('daftar',$daftar);
        $this->db->update('wali', $data);
        }



    $this->cek($daftar);

    }
}
