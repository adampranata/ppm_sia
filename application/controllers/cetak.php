<?php 

class cetak extends CI_Controller
{
	function __construct()
	{
		parent:: __construct();
		$this->load->model('profile_m');
	}

	function index() 
	{
		$data['hasil'] = $this->profile_m->get_santri();

		$this->load->view('cetak/cetak',$data);

	}
 }

 ?>